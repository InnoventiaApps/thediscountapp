package com.innoventia.thediscount.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by C9 on 9/26/2017.
 */

public class CommonModelList<T> extends StatusModel implements Serializable {
    @SerializedName("object")
    List<T> object;

    public CommonModelList(List<T> object) {
        super(0, "");
        this.object = object;
    }

    public CommonModelList(int errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    public List<T> getData() {
        return object;
    }
}
