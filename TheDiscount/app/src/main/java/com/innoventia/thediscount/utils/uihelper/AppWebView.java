package com.innoventia.thediscount.utils.uihelper;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by C9 on 10/11/2017.
 */

public class AppWebView extends WebView {
    public AppWebView(Context context) {
        super(context);
    }

    public AppWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
