package com.innoventia.thediscount.modules.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.main.interfaces.OnBindLocationOnMapListListner;
import com.innoventia.thediscount.main.mulitithread.AsynUI;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.LocationModel;
import com.innoventia.thediscount.modules.view.LocationOnMap;
import com.innoventia.thediscount.net.ApiUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUs extends BaseFragment implements OnBackgroundUIListner {


    public ContactUs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        baseView= inflater.inflate(R.layout.fragment_contact_us, container, false);

       new AsynUI(this).execute();

        return baseView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }


    public void  getLocationList() {

        ApiUtils.getContactLocation(this);
        if(LocationOnMap.commonlocationLists!=null&&LocationOnMap.commonlocationLists.getData().size()!=0)
        {
            addChildFragment(new Fragment[]{this,LocationOnMap.newInstance(ApiUtils.getContactLocation(this))},R.id.fragmentContactLocs);
        }


    }

    @Override
    public void OnLoadBackground() {

        getLocationList();

    }
}
