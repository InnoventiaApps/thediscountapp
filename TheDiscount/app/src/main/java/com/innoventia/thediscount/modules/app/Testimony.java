package com.innoventia.thediscount.modules.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class Testimony extends BaseFragment {


    public Testimony() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        baseView= inflater.inflate(R.layout.fragment_testimony, container, false);
        return baseView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }
}
