package com.innoventia.thediscount.modules.userauth;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alahammad.otp_view.OtpView;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.utils.UiUtils;
import com.innoventia.thediscount.utils.uihelper.AppTextView;
import com.innoventia.thediscount.utils.uihelper.UserAuthButton;

public class OtpDialog extends DialogFragment {

    View baseView;
    public UserAuthButton btnSubmit;
    private String otp;
    OtpView otpEdtTxt;
    private String verificationCode=null;
    private static final String VERIFICATIONCODE ="verificationCode";
    public OnDiallogAcessListener onDiallogAcessListener;
    public static final int OTP_DIALOG_FRAGMENT = 1;
    Fragment parent=null;
    AppTextView otpMsg;
    public static OtpDialog dialogFragment;
    // TODO: Rename and change types of parameters
    private String mParam1;
    public static OtpDialog newInstance(Fragment instance,String verificationCode){

        dialogFragment = new OtpDialog();
        Bundle bundle = new Bundle();
        bundle.putString(VERIFICATIONCODE, verificationCode);
        dialogFragment.setArguments(bundle);
        dialogFragment.setTargetFragment(instance,OTP_DIALOG_FRAGMENT);
        return dialogFragment;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null)
        {

            verificationCode=getArguments().getString(VERIFICATIONCODE);
            parent=getTargetFragment();
            if(parent instanceof OnDiallogAcessListener)
            {
                onDiallogAcessListener=(OnDiallogAcessListener)parent;

            }


        }

    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        baseView=inflater.inflate(R.layout.otplayout, container, false);

        btnSubmit=(UserAuthButton) baseView.findViewById(R.id.btnSubmit);
        otpMsg=(AppTextView) baseView.findViewById(R.id.btnSubmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                otp=otpEdtTxt.getOTP();
                if(!otp.equals(""))
                {
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, otp);
                onDiallogAcessListener.SigninWithPhone(credential); }
                else
                {

                    otpMsg.setText("Enter a Valid Otp!");
                   // UiUtils.setError(otpEdtTxt,"enter valid otp!");

                }

            }

        });

        return baseView;
    }


    public void setVerificationRecieved(String message)
    {
        try
        {
            otpEdtTxt.setOTP(message);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                   btnSubmit.performClick();
                }
            },1000);

        }
        catch (Exception e)
        {

            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dialogFragment=null;
        baseView=null;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public interface OnDiallogAcessListener {
        void SigninWithPhone(PhoneAuthCredential credential);

    }




}
