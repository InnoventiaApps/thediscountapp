package com.innoventia.thediscount.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.model.LoginModel;
import com.innoventia.thediscount.model.UserModel;


import java.util.ArrayList;
import java.util.List;


public class Preferences {
    private static Preferences instance;
    private static final String KEY_USER_MODEL="login_model";
    private static final String KEY_LIST="discount_list";
   /* private static final String KEY_TUTORIAL_SKIP="tutorial_skip";
*/
    private static final String KEY_MAIL_SKIP="mail_skip";
    private static final String KEY_RATE_SKIP="rate_skip";

    private static final String KEY_VERSION="version_code";
    private static final String KEY_UPDATE_NECESSARY="update_necessary";
    private static final String KEY_SORT_TYPE="sort_type";

    private SharedPreferences preferences;

    public static Preferences getInstance(Context ctx) {
        if(instance==null)
            instance=new Preferences(ctx);
        return instance;
    }
    private Preferences(Context ctx) {
        preferences=ctx.getSharedPreferences(ctx.getPackageName()+ctx.getString(R.string.app_name),Context.MODE_PRIVATE);
    }

    private void clear(String key) {
        preferences.edit().remove(key).commit();
    }

    private <T> void  addList(String key,List<T> list) {
        preferences.edit().putString(key,new Gson().toJson(list)).commit();
    }

    private <T> List<T> getList(String key, Class<T> elementClass) {
        return new Gson().fromJson(preferences.getString(key,new Gson().toJson(new ArrayList<T>())),new TypeToken<ArrayList<T>>(){}.getType());
    }

    private void setBoolean(String key,boolean value) {
        preferences.edit().putBoolean(key,value).commit();
    }

    private int getInt(String key,int defValue) {
        return preferences.getInt(key,defValue);
    }
    private void setInt(String key,int value) {
        preferences.edit().putInt(key,value).commit();
    }

    private boolean getBoolean(String key,boolean defValue) {
        return preferences.getBoolean(key,defValue);
    }

    private void addString(String key,String value) {
        preferences.edit().putString(key,value).commit();
    }

    private String getString(String key,String defValue) {
        return preferences.getString(key,defValue);
    }

    //UPDATE
    public void setUpdate(int version,boolean isNecessary) {
        setInt(KEY_VERSION,version);
        setBoolean(KEY_UPDATE_NECESSARY,isNecessary);
    }

    public boolean isUpdateNecessary() {
        return getBoolean(KEY_UPDATE_NECESSARY,false);
    }

    public int getVersion() {
        return getInt(KEY_VERSION,0);
    }

 /*   //TUTORIAL
    public void setTutorialStatus(boolean isShowed) {
        setBoolean(KEY_TUTORIAL_SKIP,isShowed);
    }

    public boolean isTutorialShowedOnce() {
        return getBoolean(KEY_TUTORIAL_SKIP,false);
    }
*/

    public void setMailVerificationStatus(boolean isDone) {
        setBoolean(KEY_MAIL_SKIP,isDone);
    }

    public boolean getMailVerificationStatus() {
        return getBoolean(KEY_MAIL_SKIP,false);
    }

    //TUTORIAL
    public void setRateStatus(boolean isShowed) {
        setBoolean(KEY_RATE_SKIP,isShowed);
    }

    public boolean getRateStatus() {
        return getBoolean(KEY_RATE_SKIP,false);
    }


    //LOGIN DETAILS
    public void setLoginDetails(LoginModel model) {
        addString(KEY_USER_MODEL,new Gson().toJson(model));
    }

    public UserModel getLoginDetails() {
        //Delete these and uncomment last line- only for demo
//        UserModel model=new UserModel();
//        model.setUserId(6);
//        return model;
        return new Gson().fromJson(preferences.getString(KEY_USER_MODEL,null),new TypeToken<UserModel>(){}.getType());
    }

    public boolean isLoggedIn() {

        boolean isLoggedIn=false;

       if(getUserid()!=0)
       {
          isLoggedIn=true;
       }
       else {
           return isLoggedIn;
       }

        return isLoggedIn;
    }

    public long getUserid() {

        UserModel model=getLoginDetails();
        long userid=0;

        if(model!=null) {

               userid= model.getUserId();
            }
            else {
                userid= 0;
            }


        return userid;
    }

    public void clearLoginDetails() {
        clear(KEY_USER_MODEL);
    }

    //PRODUCT LIST

   /* *//**
     *

     * @param layoutId   - >  For which adapter layout -> Listfragment is a common class for all list.
     *                   So layout id is needed
     */
   /* public void addProductList(List<ProductModel> list,int layoutId,String type) {
        addList(KEY_PRODUCT_LIST+layoutId+type,list);
    }

    public List<ProductModel> getProductList(int layoutId,String type) {
        return new Gson().fromJson(preferences.getString(KEY_PRODUCT_LIST+layoutId+type,new Gson().toJson(new ArrayList<ProductModel>())),new TypeToken<ArrayList<ProductModel>>(){}.getType());
    }*/

    public void clearList(int layoutId) {
        clear(KEY_LIST+layoutId);
    }

    //SORT
    public void setSortType(String sortType) {
        addString(KEY_SORT_TYPE,sortType);
    }

    public String getSortType() {
        return getString(KEY_SORT_TYPE,"relevent");
    }

    public void clearSortType() {
        clear(KEY_SORT_TYPE);
    }



}
