package com.innoventia.thediscount.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserWallet {

    @SerializedName("walletId")
    @Expose
    private int walletId;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("customerId")
    @Expose
    private int customerId;

    @SerializedName("referalCode")
    @Expose
    private String referalCode;
    @SerializedName("createdDate")
    @Expose
    private int createdDate;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("credits")
    @Expose
    private double credits;

    /**
     * No args constructor for use in serialization
     *
     */
    public UserWallet() {
    }

    public UserWallet(String referalCode) {
        this.referalCode = referalCode;
    }

    /**
     *
     * @param walletId
     * @param customerId
     * @param status
     * @param referalCode
     * @param userId
     * @param credits
     * @param createdDate
     */
    public UserWallet(int walletId, int userId, int customerId, String referalCode, int createdDate, int status, double credits) {
        super();
        this.walletId = walletId;
        this.userId = userId;
        this.customerId = customerId;
        this.referalCode = referalCode;
        this.createdDate = createdDate;
        this.status = status;
        this.credits = credits;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getReferalCode() {
        return referalCode;
    }

    public void setReferalCode(String referalCode) {
        this.referalCode = referalCode;
    }

    public int getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(int createdDate) {
        this.createdDate = createdDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getCredits() {
        return credits;
    }

    public void setCredits(double credits) {
        this.credits = credits;
    }

}
