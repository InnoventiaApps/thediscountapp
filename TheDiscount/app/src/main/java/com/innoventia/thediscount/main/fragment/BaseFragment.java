package com.innoventia.thediscount.main.fragment;

import android.arch.lifecycle.Lifecycle;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;

import com.innoventia.thediscount.main.activity.BaseActivity;
import com.innoventia.thediscount.main.interfaces.BaseActvityInterface;
import com.innoventia.thediscount.main.interfaces.BaseInterface;

import java.util.List;


/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BaseFragment#newInstance} factory method to
 * create an instance of this fragment.*/

public abstract class BaseFragment extends Fragment implements BaseActvityInterface {


    protected View baseView=null;
    protected Context context;
    protected BaseActivity parent;
    BaseActivity baseActivity;
    private static final int TASK_ID =12 ;
    private static final int TASK_BUNDLE =21 ;
    protected String TAGS="";

    Context baseContext=null;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public Lifecycle getLifecycle() {
        return super.getLifecycle();
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
    }

    @Nullable
    @Override
    public Context getContext() {
        context=super.getContext();
        return context;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        baseContext=context;
        baseActivity=(BaseActivity)baseContext;
    }

    @Override
    public void setHasOptionsMenu(boolean hasMenu) {
        super.setHasOptionsMenu(hasMenu);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View getView() {
        return super.getView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();
    }

      public interface LoadDataListener
    {

        void onLoadFragmentUi();

    }
    @Override
    public void showToastAnywhere(String message) {


       baseActivity.showToastAnywhere(message);
    }

    @Override
    public void showToastAnywhere(int message) {

        baseActivity.showToastAnywhere(message);

    }

    @Override
    public void showSnackbarAnywhere(String message) {

    }

    @Override
    public void showDialogAnyWhere(String Message) {

    }

    @Override
    public void showProgress(CharSequence msg) {

    }

    @Override
    public void showProgress(int msg) {

    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void sendIntent(@NonNull Context from, @NonNull Class[] to, boolean condition) {

        baseActivity.sendIntent(from,to,condition);
    }

    @Override
    public void sendIntent(@NonNull Context from, @NonNull Class to) {

        baseActivity.sendIntent(from,to);

    }

    protected void changeFragment( Fragment changeFrament, @IdRes int container,boolean isDirectChild)
    {
       if (isDirectChild)
       {
           changeFrament.getChildFragmentManager().beginTransaction().replace(container,changeFrament).addToBackStack(changeFrament.getClass().getSimpleName()).commit();
       }
       else
       {
           baseActivity.changeFragment(changeFrament,container);
       }
    }


    protected void addChildFragment( Fragment[] fragments, @IdRes int container)
    {
         fragments[0].getChildFragmentManager().beginTransaction().add(container,fragments[1]).addToBackStack(fragments[1].getClass().getSimpleName()).commit();

    }

    public void dismissDialogue(DialogFragment dialogFragment) {

        if(dialogFragment !=null)
            if(dialogFragment.isAdded())
                dialogFragment.dismiss();

    }

}


