package com.innoventia.thediscount.modules.view.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.interfaces.OnAdapterInitialyzeListener;
import com.innoventia.thediscount.main.interfaces.OnAdapterResponseListener;
import com.innoventia.thediscount.modules.view.adapter.GenericViewHolder;
import java.util.List;

/**
 * Base generic RecyclerView adapter.
 * Handles basic logic such as adding/removing items,
 * setting listener, binding ViewHolders.
 * Extend the adapter for appropriate use case.
 *
 * @param <T>  type of objects, which will be used in the adapter's dataset
/* * @param <L>  click listener {@link OnAdapterInitialyzeListener}
 //* @param <VH> ViewHolder {@link GenericViewHolder}
 */

//public abstract class GenericRecyclerAdapter<T, L extends OnAdapterInitialyzeListener ,VH extends GenericRecyclerAdapter.GenericViewHolder> extends RecyclerView.Adapter<VH> {
public class GenericRecyclerAdapter<T> extends RecyclerView.Adapter<com.innoventia.thediscount.modules.view.adapter.GenericViewHolder> implements OnAdapterResponseListener {

    List<T> items;
    GenericViewHolder genericViewHolder;
   // VH viewHolder;
    int listItemResource;
    public GenericRecyclerAdapter(GenericViewHolder viewHolder,List<T> items) {

        this.genericViewHolder = viewHolder;
        this.items = items;
       // this.listener =(L)genericViewHolder;

    }

  /*  public GenericRecyclerAdapter(VH viewHolder, List<T> items, @LayoutRes int listItemResource) {
        this.genericViewHolder = viewHolder;
        this.items = items;
        this.listItemResource = listItemResource;

    }*/

    @NonNull
    @Override
    public com.innoventia.thediscount.modules.view.adapter.GenericViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

     //   View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(listItemResource, null);

        //VH rcv = (VH) new GenericViewHolder(layoutView);

        com.innoventia.thediscount.modules.view.adapter.GenericViewHolder rcv = (com.innoventia.thediscount.modules.view.adapter.GenericViewHolder) genericViewHolder;


        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull com.innoventia.thediscount.modules.view.adapter.GenericViewHolder genericViewHolder, int position) {


        genericViewHolder.onBind(items,position);

    }



    @Override
    public int getItemCount() {

        return items != null ? items.size() : 0;

    }

    @Override
    public void onViewClick(View view, int adapterPosition) {

    }

    @Override
    public void onPagination() {

    }
}




        /*public void onBind(int position)
        {
            try {
                T typeItem=(T)items.get(position);

                CategoryModel categoryModel = (CategoryModel) categories.get(position);
                categoryName.setText(categoryModel.getCategoryName());
                Log.d("CATEGORY_NAME = ", categoryModel.getCategoryName());
                ApiUtils.loadImage(context,categoryModel.getCategoryIcon(), categoryIcon);
                Log.d("CATEGORY_ICON = ", String.valueOf(uri));

            }catch (Exception e){
                e.printStackTrace();
            }     }
        }*/



