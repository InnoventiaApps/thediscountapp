package com.innoventia.thediscount.modules.view.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.interfaces.OnAdapterInitialyzeListener;
import com.innoventia.thediscount.main.interfaces.OnAdapterResponseListener;
import com.innoventia.thediscount.model.CategoryModel;
import com.innoventia.thediscount.net.ApiUtils;

import java.util.List;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.HomeCategoryViewHolder> {

    Context context;
    List<CategoryModel> categories;
    OnAdapterInitialyzeListener onAdapterListener;
    Uri uri;
    String string_uri;

    public HomeCategoryAdapter(Context context, List<CategoryModel> categories, OnAdapterInitialyzeListener onAdapterListener) {

        this.context=context;
        this.categories=categories;
        this.onAdapterListener = onAdapterListener;
        if (onAdapterListener != null && (onAdapterListener instanceof Fragment))
            context = ((Fragment) onAdapterListener).getActivity();
        else if (onAdapterListener != null && (onAdapterListener instanceof Context))
            context = (Context) onAdapterListener;
    }

    public HomeCategoryAdapter(Context context, List<CategoryModel> categories) {
        this.context=context;
        this.categories=categories;
    }


    public void clear() {
        onAdapterListener = null;
        context = null;
        categories.clear();
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public HomeCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.homecategory, null);
        HomeCategoryViewHolder rcv = new HomeCategoryViewHolder(layoutView);
        Log.d("onCreateViewHolder= ","HomeCategoryViewHolder");
        return rcv;

    }

    @Override
    public void onBindViewHolder(@NonNull HomeCategoryViewHolder homeCategoryViewHolder, int position) {

        homeCategoryViewHolder.onBind(position);

    }

    @Override
    public int getItemCount() {

        return categories != null ? categories.size() : 0;

    }

/*    @Override
    public void initialise() {

        MakeListFragment.adapter=this;
        MakeListFragment.onAdapterResponseListener=this;
      // MakeListFragment.list=(List<Object>)categories;
    }*/




     class HomeCategoryViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        ImageView categoryIcon;
        private boolean isCallingNewList = false;

        public HomeCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.tvCategoryLabel);

            categoryIcon = itemView.findViewById(R.id.imagCategoryIcon);

        }

        public void onBind(int position)
        {
            try {
                CategoryModel categoryModel = (CategoryModel) categories.get(position);
                categoryName.setText(categoryModel.getCategoryName());
                Log.d("CATEGORY_NAME = ", categoryModel.getCategoryName());
                ApiUtils.loadImage(context,categoryModel.getCategoryIcon(), categoryIcon);
                Log.d("CATEGORY_ICON = ", String.valueOf(uri));

            }catch (Exception e){
                e.printStackTrace();
            }     }   }

}



