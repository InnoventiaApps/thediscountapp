/*
package com.innoventia.thediscount;

*/
/*import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;*//*



public class test {



    package com.nallameenapp.nallameen.modules.others;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nallameenapp.nallameen.R;
import com.nallameenapp.nallameen.fragments.BaseFragment;
import com.nallameenapp.nallameen.interfaces.OnMailListener;
import com.nallameenapp.nallameen.model.CommonModelList;
import com.nallameenapp.nallameen.model.LocationModel;
import com.nallameenapp.nallameen.modules.checkout.DeliveryLocationActivity;
import com.nallameenapp.nallameen.modules.home.adapters.ListAdapterPopup;
import com.nallameenapp.nallameen.net.ApiUtils;
import com.nallameenapp.nallameen.net.Client;
import com.nallameenapp.nallameen.net.MailHelper;
import com.nallameenapp.nallameen.net.endpoint.ApiService;
import com.nallameenapp.nallameen.util.Utilities;
import com.nallameenapp.nallameen.util.Validation;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

    */
/**
     * A simple {@link Fragment} subclass.
     *//*

    public class DeliveryLocationsFragment extends BaseFragment implements OnMapReadyCallback,OnMailListener {

        @BindView(R.id.map)
        MapView mapView;

        @BindView(R.id.layoutDeliveryBottom)
        View layoutDeliveryBottom;

        EditText edtName;
        EditText edtPhone;
        TextView txtPinOne;
        EditText edtPinTwo;

        TextView txtCity;
        TextView txtLocation;
        TextView txtDeliveryLocation;

        RadioGroup rgTime;

        android.support.v7.widget.ListPopupWindow popupWindow;



        private GoogleMap googleMap;

        private Dialog deliveryDialog;

        private boolean isCallingLocation=false;
        private LocationModel deliveryLocation;
        private Place deliveryPlace;

        private List<LocationModel> locationModels;

        private static final int PERMISSION_LOCATION=15;
        private static final int PERMISSION_PICK_LOCATION=2;
        private static final int REQUEST_LOCATION=150;
        private static final int REQUEST_DELIVERY_LOCATION=151;
        private static final int REQUEST_MAIL=17;




        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v= inflater.inflate(R.layout.frag_delivery_locations, container, false);
            try {
                getAllLocations();
                unbinder=ButterKnife.bind(this, v);
                MapsInitializer.initialize(this.getActivity());
                mapView.onCreate(savedInstanceState);
                mapView.getMapAsync(this);
            } catch (Throwable e) {
            }
            return v;
        }

        @Override
        public void onResume() {
            super.onResume();
            if (mapView != null) {
                mapView.onResume();
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            if (mapView != null) {
                mapView.onPause();
            }
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            if (mapView != null) {
                mapView.onDestroy();
            }
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if(requestCode==PERMISSION_LOCATION && grantResults.length==2 && (grantResults[0]== PackageManager.PERMISSION_GRANTED || grantResults[1]==PackageManager.PERMISSION_GRANTED)){
                if (Utilities.hasLocationPermission(getActivity(), PERMISSION_LOCATION, true))
                    googleMap.setMyLocationEnabled(true);
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            try {
                if(resultCode== Activity.RESULT_OK) {
                    switch (requestCode) {
                        case REQUEST_DELIVERY_LOCATION:deliveryLocation= (LocationModel) data.getSerializableExtra(DeliveryLocationActivity.EXTRA_LOCATION);
                            txtDeliveryLocation.setText(deliveryLocation.getLocation());
                            txtPinOne.setText(deliveryLocation.getPincode());
                            break;
                        case REQUEST_LOCATION:
                            deliveryPlace = PlacePicker.getPlace(getActivity(), data);
                            txtLocation.setText(deliveryPlace.getAddress());
                            break;
                    }
                }
            } catch (Exception e) {
            }
            isCallingLocation=false;
        }

        @OnClick(R.id.imgDeliveryForm)
        void showContactDialog() {
            if(deliveryDialog==null) {
                deliveryDialog = new Dialog(getActivity(), R.style.FullDilaog);
                deliveryDialog.setContentView(R.layout.layout_dia_delivery);
                edtName = (EditText) deliveryDialog.findViewById(R.id.edtDiaDeliveryName);
                txtPinOne =  deliveryDialog.findViewById(R.id.txtDiaDeliveryLocPin);
                edtPhone = (EditText) deliveryDialog.findViewById(R.id.edtDiaDeliveryPhone);
                edtPinTwo = (EditText) deliveryDialog.findViewById(R.id.edtDiaDeliveryPin);

                txtCity = (TextView) deliveryDialog.findViewById(R.id.txtDiaDeliveryCity);
                txtLocation = (TextView) deliveryDialog.findViewById(R.id.txtDiaLocation);
                txtDeliveryLocation = (TextView) deliveryDialog.findViewById(R.id.txtDiaDeliveryLocation);
                rgTime = (RadioGroup) deliveryDialog.findViewById(R.id.rgDiaDelivery);

                deliveryDialog.findViewById(R.id.btnDiaDeliverySubmit).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        submitForm();
                    }
                });
                deliveryDialog.findViewById(R.id.imgDiaDeliveryClose).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        closeDialog();
                    }
                });
                deliveryDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        Utilities.hideKeyBoard(deliveryDialog);
                        if(popupWindow!=null && popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                        layoutDeliveryBottom.setVisibility(View.VISIBLE);
                    }
                });
                deliveryDialog.findViewById(R.id.txtDiaLocation).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utilities.hideKeyBoard(deliveryDialog);
                        getLocation();
                    }
                });
                deliveryDialog.findViewById(R.id.layoutDiaDeliveryCity).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utilities.hideKeyBoard(deliveryDialog);
                        if(popupWindow==null) {
                            popupWindow = new android.support.v7.widget.ListPopupWindow(getActivity());

                            popupWindow.setAnchorView(deliveryDialog.findViewById(R.id.layoutDiaDeliveryCity));
                            List<String> data = Arrays.asList(getResources().getStringArray(R.array.delivery_city_list));
                            popupWindow.setAdapter(new ListAdapterPopup(data));
                            popupWindow.setModal(true);
                            popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    txtCity.setText(getResources().getStringArray(R.array.delivery_city_list)[position]);
                                    popupWindow.dismiss();
                                }
                            });

                        }
                        popupWindow.show();
                    }
                });
                deliveryDialog.findViewById(R.id.layoutDeliverySearch).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utilities.hideKeyBoard(deliveryDialog);
                        if(!isCallingLocation) {
                            isCallingLocation=true;
                            getActivity().startActivityFromFragment(DeliveryLocationsFragment.this, new Intent(getActivity(), DeliveryLocationActivity.class), REQUEST_DELIVERY_LOCATION);
                        }
                    }
                });
            }
            layoutDeliveryBottom.setVisibility(View.GONE);
            edtPhone.setText("");
            edtPinTwo.setText("");
            txtLocation.setText("");
            edtName.setText("");
            txtPinOne.setText("");
            txtDeliveryLocation.setText("");

            deliveryDialog.show();
            Utilities.hideKeyBoard(deliveryDialog);

        }


        private void getAllLocations() {
            Call<CommonModelList<LocationModel>>  call= Client.getApiClient().create(ApiService.class)
                    .getDeliveryLocations(ApiUtils.getApiParams(new String[]{"search"},new String[]{""}));
            call.enqueue(new Callback<CommonModelList<LocationModel>>() {
                @Override
                public void onResponse(Call<CommonModelList<LocationModel>> call, Response<CommonModelList<LocationModel>> response) {

                    if(!isDetached()) {
                        try {
                            locationModels=response.body().getData();
                            setMarkers();
                        } catch (Exception e) {
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonModelList<LocationModel>> call, Throwable t) {
                    if(!isDetached()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(!isDetached()) {
                                    getAllLocations();
                                }
                            }
                        },3000);

                    }
                }
            });
        }

        synchronized void setMarkers() {
            if(locationModels!=null && googleMap!=null) {
                for(LocationModel m:locationModels) {
                    try {
                        MarkerOptions marker = new MarkerOptions().position(new LatLng(m.getLat(),m.getLng()))
                                .title(m.getLocation());
                        marker.icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                        googleMap.addMarker(marker);
//                    showMessage("sdsss","="+isDetached()+"   "+m.toString(),"dsfdsf");
//                    break;
                    } catch (Exception e) {

                    }

                }
            }
        }

        void getLocation() {
            if (Utilities.hasLocationPermission(getActivity(), PERMISSION_PICK_LOCATION, true)) {
                try {
                    if(isCallingLocation) {
                        return;
                    }
                    isCallingLocation=true;
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    getActivity().startActivityFromFragment(this, builder.build(getActivity()), REQUEST_LOCATION);
                } catch (Exception e) {
                    isCallingLocation=false;
                }
            }
        }


        private void submitForm() {
            Utilities.hideKeyBoard(getActivity());
            if(isCallingWeb)
                return;
            isCallingWeb=true;
            if(!Validation.checkIsEmpty(edtName,edtPhone,txtLocation,edtPinTwo)
                    && Validation.isAValidMobile(edtPhone)
                    && Validation.isAvalidName(edtName)
                    && !Validation.checkIsHavingSpecialChars(edtName)) {
                submit();
                return;
            }
            isCallingWeb=false;
        }

        private void submit() {
            Utilities.hideKeyBoard(getActivity());
            showProgress(R.string.info_submitting);
            LatLng latLng=deliveryPlace.getLatLng();
            String text="Name : "+edtName.getText().toString()
                    +"<br>Phone : "+edtPhone.getText().toString()
                    +"<br>Location : "+deliveryPlace.getAddress()
                    +"<br>Lattitude : "+latLng.latitude
                    +"<br>Longitude : "+latLng.longitude
                    +"<br>Pin : "+edtPinTwo.getText().toString();
            new MailHelper(getActivity(),this,REQUEST_MAIL,null,null,
                    getStringRes(R.string.hint_delivery_location),text,null,null).execute();
        */
/*showProgress(R.string.info_submitting);
        Client.getApiClient().create(ApiService.class)
                .contactUs(ApiUtils.getApiParams(new String[]{},new String[]{})).enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                dismissProgress();
                isCallingWeb=false;
                try {
                    if(response.body().isSuccess()) {
                        showMessage(R.string.tit_success,response.body().getMsg(),R.string.tit_ok);
                        closeDialog();
                    } else {
                        showMessage(R.string.tit_error,response.body().getMsg(),R.string.tit_ok);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                dismissProgress();
                isCallingWeb=false;
                onInternetError(t);
            }
        });*//*

        }


        private void closeDialog() {
            deliveryDialog.dismiss();
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            try {
                this.googleMap=googleMap;
                if (Utilities.hasLocationPermission(getActivity(), PERMISSION_LOCATION, true))
                    googleMap.setMyLocationEnabled(true);
                LatLng latLng=new LatLng( 9.984303,
                        76.297861);//ernakulam
                CameraUpdate center=
                        CameraUpdateFactory.newLatLng(latLng);
                CameraUpdate zoom=CameraUpdateFactory.zoomTo(12);

                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);


//            MarkerOptions options=new MarkerOptions().position(latLng);
//            googleMap.addMarker(options);

            } catch (Throwable e) {
            }
            setMarkers();
        }

        @Override
        public void onMailSent(int requestId, boolean isSuccess) {
            if(requestId!=REQUEST_MAIL) {
                return;
            }
            isCallingWeb=false;
            dismissProgress();
            try {
                if(isSuccess) {
                    showMessage(R.string.tit_success,R.string.info_request_submitted,R.string.tit_ok);
                    deliveryDialog.dismiss();
                } else {
                    showMessage(R.string.tit_error,R.string.msg_error,R.string.tit_ok);
                }
            } catch (Exception e) {
            }
        }
    }



}

*/
/*



    public class HRMSActivity extends BaseActivity
            implements NavigationView.OnNavigationItemSelectedListener {

        DrawerLayout drawer;
        ActionBarDrawerToggle toggle;
        FloatingActionButton fab;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_hrms);

            fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        }

        @Override
        public void onBackPressed() {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.hrm, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the LocationOnMap/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            // Handle navigation view item clicks here.
            int id = item.getItemId();

            if (id == R.id.nav_camera) {
                // Handle the camera action
            } else if (id == R.id.nav_gallery) {

            } else if (id == R.id.nav_slideshow) {

            } else if (id == R.id.nav_manage) {

            } else if (id == R.id.nav_share) {

            } else if (id == R.id.nav_send) {

            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }


        private void selectSideDrawer(MenuItem item)
        {

            switch (item.getItemId()) {

                case R.id.nav_home:
                    changeFragment(HomeFragment.newInstance(), R.string.cochin, true);
                    break;
                case R.id.nav_prof:
                    changeFragment(new ProfileFragment(), R.string.profile, false);
                    break;

                case R.id.nav_cart:
                    changeFragment(CartFragment.newInstance(cartItemCount), R.string.cart, false);
                    break;

                case R.id.nav_oder:
                    changeFragment(new OrderHistoryFragment(), R.string.history, false);
                    break;

                case R.id.nav_favrt:
                    break;

                case R.id.nav_sell:
                    changeFragment(new SellWithUsFragment(), R.string.sell_with_us, false);
                    break;

                case R.id.nav_referral:changeFragment(new ShareFragment(),R.string.referral_money,false);
                    break;
                case R.id.nav_notfcn:
                    break;

                case R.id.nav_support:
                    changeFragment(new SupportFragment(), R.string.support, false);
                    break;
                case R.id.nav_help:
                    changeFragment(SupportFragment.newInstance(true), R.string.help, false);
                    break;

                case R.id.nav_logout:
                    showMessage(this, ALERT_LOGOUT, R.string.logout, R.string.warn_logout, R.string.logout, R.string.cancel, true);
                    break;
            }
            item.setChecked(true);
            drawer.closeDrawer(GravityCompat.START);

        }

    }
*//*

}
*/
