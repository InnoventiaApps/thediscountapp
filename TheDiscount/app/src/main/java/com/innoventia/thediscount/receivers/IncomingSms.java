package com.innoventia.thediscount.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.innoventia.thediscount.data.Preferences;
import com.innoventia.thediscount.main.activity.BaseActivity;
import com.innoventia.thediscount.modules.userauth.OtpDialog;
import com.innoventia.thediscount.modules.userauth.SignUp;

public class IncomingSms extends BroadcastReceiver 
{
    String verificationCode;
    public OtpDialog dialogFragment;
    public IncomingSms(String verificationCode) {
        this.verificationCode=verificationCode;
    }

    @Override
  public void onReceive(Context context, Intent intent) 
  {

  final Bundle bundle = intent.getExtras();
  try {
  if (bundle != null) 
  {
   final Object[] pdusObj = (Object[]) bundle.get("pdus");
   for (int i = 0; i < pdusObj .length; i++) 
   {
    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[])                                                                                                    pdusObj[i]);
    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
    String senderNum = phoneNumber ;
    String message = currentMessage .getDisplayMessageBody();
    try
    {
        // number
     if (senderNum .equals(Preferences.getInstance(context).getLoginDetails().getMobile()))
     {
         if(OtpDialog.dialogFragment!=null)
            dialogFragment=OtpDialog.dialogFragment;
           if(message.contains(verificationCode))
           {
             // message.substring(verificationCode.);
               dialogFragment.setVerificationRecieved(verificationCode);


           }
           dialogFragment.setVerificationRecieved(message);
     }
  }
  catch(Exception e){}
  
  }
   }

   } catch (Exception e) 
  {
                
 }
 }

}
