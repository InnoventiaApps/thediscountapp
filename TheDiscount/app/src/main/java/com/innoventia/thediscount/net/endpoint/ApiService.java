package com.innoventia.thediscount.net.endpoint;


import com.innoventia.thediscount.model.CategoryModel;

import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.CustomerDetails;
import com.innoventia.thediscount.model.LoginModel;
import com.innoventia.thediscount.model.MerchantModel;


import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by user on 03-07-2017.
 */

public interface ApiService {

   /* @Headers({"Content-Type:application/json"})
    @POST("input/getallwishlist")
    Call<CommonModelList<WishListModel>>getAllWishList(@Body Map<String, String> remove_wishlist_params);

    @Headers({"Content-Type:application/json"})
    @POST("order/addtowishlist")
    Call<CommonModelList<WishListModel>>removeWishList(@Body Map<String, String> remove_wishlist_params);

    @Headers({"Content-Type:application/json"})
    @POST("order/addtowishlist")
    Call<CommonModelList<WishListModel>>addWishList(@Body Map<String, String> add_wishlist_params);

    @Headers({"Content-Type:application/json"})
    @POST("input/getalltestimonials")
    Call<CommonModelList<TestimonyModel>>getallTestimonials(@Body Map<String, String> testimoni_params);
*/
    @Headers({"Content-Type:application/json"})
    @POST("input/savetestimonial")
    Call<CommonModelList>postFeedback(@Body Map<String, String> feedback_params);

    @Headers({"Content-Type:application/json"})
    @POST("order/makeappoinment")
    Call<CommonModelList>makeAppointment(@Body Map<Long, Long> appmnt_params);

   @Headers({"Content-Type:application/json"})
    @POST("user/getallnearestmerchants")
   Call<CommonModelList<MerchantModel>> searchMerchants(@Body Map<String, String> allmerchants_params);

  /*  @Headers({"Content-Type:application/json"})
            @POST("input/getallappoinments")
    Call<CommonModelList<AppointmentModel>>getAppointments(@Body Map<String, String> appointment_params);*/

    @Headers({"Content-Type:application/json"})
    @POST("order/deleteappoinment")
    Call<CommonModelList>deleteAppointmet(@Body Map<String, String> delete_appmnt_params);


    @Headers({"Content-Type:application/json"})
    @POST("user/savecustomer")
    Call<CommonModelList> getRegistrationDetails(@Body Map<String, String> reg_params);

    @Headers({"Conten-Type:application/json"})
    @POST("user/savecustomer")
    Call<CommonModelList> getRegistrationDetails_Data(@Body CustomerDetails customerDetails);

    @Headers({"Content-Type:application/json"})
    @POST("user/login")
    Call<LoginModel> getLoginDetails(@Body Map<String, String> login_params);


    @Headers({"Content-Type:application/json"})
    @POST("user/getallmerchants")
    Call<CommonModelList<MerchantModel>> getAllMerchants(@Body Map<String, String> parameters);


    @GET("input/getcategory")
    Call<CommonModelList<CategoryModel>> getCategory(@Query("sortedValue") String sortedValue
            , @Query("categoryId") String categoryId
            , @Query("status") String status
            , @Query("rowPerPage") String rowPerPage
            , @Query("currentIndex") String currentIndex);

//    @GET("input/getcategory")
//    Call<CategoryModel> getCategory(@Query("sortedValue") String sortedValue
//            ,@Query("categoryId") String categoryId
//            ,@Query("status") String status
//            ,@Query("rowPerPage") String rowPerPage
//            ,@Query("currentIndex") String currentIndex);

/*
    @POST("app/restapi/productSearchList")
    Call<ProductListBean> getSearchList(@Body HashMap<String, String> params);

    @POST("app/restapi/notificationList")
    Call<ProductListBean> getOffers(@Body HashMap<String, String> params);



    @POST("app/restapi/notificationList")
    Call<ShoppingCartMainModel> getNotifications(@Body HashMap<String, String> params);

    @POST("json/get_users/slider")
    Call<CommonModelList<BannerModel>> getSliderImages();




    */
/**
     * to get total added cart items count
     *
     * @param params ->  userId
     *//*

    @POST("app/restapi/myOrders")
    Call<MyOrdersBean> getMyOrders(@Body HashMap<String, String> params);


    //LOGIN
    @POST("json/get_users/email_validation")
    Call<StatusModel> canRegisterEmail(@Body HashMap<String, String> params);

    @POST("json/get_users/user_registration")
    Call<CommonModel<UserModel>> register(@Body HashMap<String, String> params);

    @POST("json/get_users/login_user")
    Call<CommonModel<UserModel>> login(@Body HashMap<String, String> params);

    @POST("json/get_users/forgot_password")
    Call<StatusModel> forgotPassword(@Body HashMap<String, String> params);

    @POST("json/get_users/update_user")
    Call<CommonModel<UserModel>> updateUser(@Body HashMap<String, String> params);

    //SHOPPING CART
    @POST("app/restapi/addToCart")
    Call<AddProductResponse> addToCart(@Body HashMap<String, String> params);

    @POST("app/restapi/updateCart")
    Call<AddProductResponse> updateCart(@Body HashMap<String, String> params);

    @POST("app/restapi/shoppingCart")
    Call<ShoppingCartMainModel> removeFromCart(@Body HashMap<String, String> params);
    */
/**
     * to get total added cart items count
     *
     * @param params ->  userId
     *//*

    @POST("app/restapi/getShoppingcartCount")
    Call<CartCountModel> getCartCount(@Body HashMap<String, String> params);

    @POST("app/restapi/placeOrder")
    Call<StatusModel> placeOrder(@Body HashMap<String, String> params);


    //OTHERS
    @POST("json/get_users/sell_with_us")
    Call<CommonModel<StatusModel>> sellWithUs(@Body HashMap<String, String> params);

    @POST("json/get_users/select_myaddress")
    Call<CommonModel<AddressModel>> getAddressList(@Body HashMap<String, String> params);

    @POST("json/get_users/add_new_address")
    Call<CommonModelList<StatusModel>> addAddress(@Body HashMap<String, String> params);

    @POST("json/get_users/getLocation")
    Call<CommonModelList<MerchantModel>> getDeliveryLocations(@Body HashMap<String, String> params);

    @POST("json/get_users/contact_us")
    Call<StatusModel> contactUs(@Body HashMap<String, String> params);
*/


}




