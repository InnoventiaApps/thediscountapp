package com.innoventia.thediscount.modules.userauth;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.activity.BaseActivity;
import com.innoventia.thediscount.main.activity.MainActivity;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.net.ApiUtils;
import com.innoventia.thediscount.receivers.IncomingSms;
import com.innoventia.thediscount.utils.UiUtils;
import com.innoventia.thediscount.utils.uihelper.AppButtonView;
import com.innoventia.thediscount.utils.uihelper.AppTextInputEditText;
import com.innoventia.thediscount.utils.uihelper.AppTextView;
import com.innoventia.thediscount.utils.uihelper.UserAuthButton;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SignUp.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignUp#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUp extends BaseFragment implements OnBackgroundUIListner ,OtpDialog.OnDiallogAcessListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String TAG=this.getClass().getSimpleName();
    private OnFragmentInteractionListener mListener;
    private FirebaseAuth mAuth;
   // CountryCodePicker ccp;
    UserAuthButton btnRegister;
    private boolean codeSent=false;
    private  String verificationCode;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationStateChangedCallbacks;
    private boolean mVerificationInProgress=false;
    private FirebaseUser user;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DialogFragment dialogFragment;
    AppTextInputEditText txtFirstname,txtLastname,txtEmail,txtPhone,txtDob,txtGender,txtPsd,txtRePsd,txtZipcode,txtReferalCode;
    String first_name,last_name,email,phone,dob,zip_code,password,confirm_pswrd,referalCode;
    int gender;
    DatePickerDialog datePickerDialog;
    AppTextView txtVerification; ;
    private String phoneNumber;
    private static final int OTP_TIME_OUT_DURATION=60;
    Spinner genderSpinner;
    ImageView checkCaptcha;
    List<String> gender_spinarray =  new ArrayList<String>();
    HashMap<String,Integer> hMap=new HashMap<String,Integer>();
    RequestQueue queue;
    IncomingSms smsReciever;
    FirebaseUser currentUser;
    com.innoventia.thediscount.utils.uihelper.AppButtonView dob_edit;

    public static final SimpleDateFormat dmyFormat = new SimpleDateFormat("dd-mm-yyyy", Locale.US);

    public SignUp() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignUp.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUp newInstance(String param1, String param2) {
        SignUp fragment = new SignUp();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        queue = Volley.newRequestQueue(getActivity());
        initFireBase();
        Toast.makeText(getActivity(),"Signup",Toast.LENGTH_LONG).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       baseView= inflater.inflate(R.layout.fragment_sign_up, container, false);
        txtVerification=baseView.findViewById(R.id.txtVerification);
       // ccp = (CountryCodePicker)baseView.findViewById(R.id.ccp);
        btnRegister=(UserAuthButton) baseView.findViewById(R.id.btnRegister);
        txtFirstname =(AppTextInputEditText) baseView.findViewById(R.id.txtFirstname);
        txtLastname =(AppTextInputEditText) baseView.findViewById(R.id.txtLastname);
        txtEmail =(AppTextInputEditText) baseView.findViewById(R.id.txtEmail);
        txtPhone =(AppTextInputEditText) baseView.findViewById(R.id.txtPhone);
        txtDob =(AppTextInputEditText) baseView.findViewById(R.id.txtDob);
        checkCaptcha=(ImageView) baseView.findViewById(R.id.checkImage);
        txtZipcode =(AppTextInputEditText) baseView.findViewById(R.id.txtZip);
        txtPsd =(AppTextInputEditText) baseView.findViewById(R.id.txtPsd);
        txtRePsd =(AppTextInputEditText) baseView.findViewById(R.id.txtRePsd);
        txtReferalCode=(AppTextInputEditText) baseView.findViewById(R.id.txtRefferal);
        genderSpinner = (Spinner)baseView. findViewById(R.id.spnGender);
        dob_edit = (AppButtonView)baseView. findViewById(R.id.dob_edit);
       // ccp.setCountryForPhoneCode(91);
      //  ccp.setCountryForNameCode("IN");

       // String code = ccp.getSelectedCountryCodeWithPlus();
      //  String code1 = ccp.getSelectedCountryCode();
      //  String c=ccp.getSelectedCountryNameCode();
      //  sharedPreferenceHelper.setCountryCode(code);*/
        gender_spinarray.add(" I am.. ");
        gender_spinarray.add("Male");
        gender_spinarray.add("Female");
        gender_spinarray.add("Trans Gender");
        hMap.put("--I am--",0);
        hMap.put("Male",1);
        hMap.put("Female",2);
        hMap.put("Trans Gender",3);

        genderSpinner.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner,gender_spinarray));
        setListeners();
        return baseView;
    }

   /* public void selectDate() {
         datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                       *//* view.setMinDate(System.currentTimeMillis() - 1000);*//*
                       // view.setMaxDate(System.currentTimeMillis());
                        txtDob.setText(dayOfMonth + " " + (monthOfYear + 1) + " " + year);

                    }
                }, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
      //  datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }*/

    private void selectDate() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                       // view.setMinDate(System.currentTimeMillis() );
                       // 29-05-1994
                        txtDob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
         datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() +1000);
      //  datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }



    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {

        codeSent = false;
        mAuth.signInWithCredential(phoneAuthCredential).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                 if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            OnLoadBackground();
                        }
                    },1000);
                    showToastAnywhere("Verification Complete");

                } else {
                    // Log.w(TAG, "signInWithCredential:failure", task.getException());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        showToastAnywhere("Invalid Verification");


                    }
                }


            }
        });



    }


    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
      /*  if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnLoadBackground() {


        // registration api vilikuka
        signUpTask();
        dismissDialogue(dialogFragment);
        sendIntent(getActivity(),MainActivity.class);
        getActivity().finish();

    }

    private void setListeners() {

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateData();
                referalCode=UiUtils.getText(txtReferalCode);

                Log.d("first_name",""+TextUtils.isEmpty(first_name));
                Log.d("first_nameEdt",""+UiUtils.getText(txtFirstname));
                signUpTask();

                if( ((Animatable) checkCaptcha.getDrawable()).isRunning()) {

                    ((Animatable) checkCaptcha.getDrawable()).stop();// Code to display your message.
                }



               /* if(!UiUtils.isEmpty(new TextInputEditText[]{txtFirstname,txtLastname,txtEmail,txtPhone,txtDob,txtGender,txtPsd,txtRePsd}))
                {
                        if(UiUtils.isValidMail(UiUtils.getText(txtEmail)) && (UiUtils.isValidMobile(UiUtils.getText(txtPhone)))){*/
                          /*  first_name      = UiUtils.getText(txtFirstname);
                            last_name       = UiUtils.getText(txtLastname); txtLastname.getText().toString();
                            phone           = UiUtils.getText(txtPhone); txtPhone.getText().toString();
                            dob             = UiUtils.getText(txtDob); txtDob.getText().toString();
                            gender          = genderSpinner.getSelectedItem().toString();txtGender.getText().toString();
                            zip_code        = UiUtils.getText(txtDob);txtZipcode.getText().toString();
                            password        = UiUtils.getText(txtDob);txtPsd.getText().toString();
                            confirm_pswrd   =UiUtils.getText(txtDob); txtRePsd.getText().toString();
                            phoneNumber=txtPhone.getText().toString();*/








                          /*  if(!UiUtils.isEmpty(new TextInputEditText[]{txtPhone})) {

                                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                                        UiUtils.getText(txtPhone),                     // Phone number to verify
                                        OTP_TIME_OUT_DURATION,                           // Timeout duration
                                        TimeUnit.SECONDS,                // Unit of timeout
                                        getActivity(),        // Activity (for callback binding)
                                        verificationStateChangedCallbacks);                      // OnVerificationStateChangedCallbacks

                            }*/


                        }
                  //  }
              //  }
            //}
        });


        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String item = adapterView.getItemAtPosition(position).toString();
                gender = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dob_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectDate();
            }
        });


        checkCaptcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   ((Animatable) checkCaptcha.getDrawable()).start();

/*
                if( ((Animatable) checkCaptcha.getDrawable()).isRunning()) {

                    ((Animatable) checkCaptcha.getDrawable()).stop();// Code to display your message.

                }*/

                SafetyNet.getClient(getActivity()).verifyWithRecaptcha(getString(R.string.CAPTCHA_API_SITE_KEY)).addOnSuccessListener(getActivity(), new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse recaptchaTokenResponse) {

                        if (!recaptchaTokenResponse.getTokenResult().isEmpty()) {

                            handleCaptchaResult(recaptchaTokenResponse.getTokenResult());
                        }

                    }
                }).addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.d(TAG, "Error message: " +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d(TAG, "Unknown type of error: " + e.getMessage());
                        }

                    }
                });

            }
        });

    }

    private void validateData() {

        if(!UiUtils.isEmpty(txtFirstname))
        {
            first_name= UiUtils.getText(txtFirstname);
        }
        else
        {
            UiUtils.setError(txtFirstname,R.string.msg_empty_error);
        }
        if(!UiUtils.isEmpty(txtLastname))
        {
            last_name= UiUtils.getText(txtLastname);
        }
        else
        {
            UiUtils.setError(txtLastname,R.string.msg_empty_error);
        }
        if(!UiUtils.isEmpty(txtDob))
        {
            dob= UiUtils.getText(txtDob);
        }
        else
        {

            UiUtils.setError(txtDob,R.string.msg_empty_error);
        }

        if(!UiUtils.isEmpty(txtZipcode))
        {

            zip_code=UiUtils.getText(txtZipcode);

        }
        else
        {
            UiUtils.setError(txtZipcode,R.string.msg_empty_error);

        }

        if(!UiUtils.isEmpty(txtEmail))
        {

           if(UiUtils.isValidMail(UiUtils.getText(txtEmail)))
           {

               email=UiUtils.getText(txtEmail);

           }

           else
           {
               UiUtils.setError(txtEmail,R.string.invEmail);
           }

        }

        else
        {

            UiUtils.setError(txtEmail,R.string.msg_empty_error);
        }


        if(!UiUtils.isEmpty(txtPhone))
        {

            if(UiUtils.isValidMobile(UiUtils.getText(txtPhone)))
            {

                email=UiUtils.getText(txtPhone);

            }

            else
            {
                UiUtils.setError(txtPhone,R.string.invMob);
            }

        }
        else
        {

            UiUtils.setError(txtPhone,R.string.msg_empty_error);
        }


        if(!UiUtils.isEmpty(txtDob))
        {

            dob=UiUtils.getText(txtDob);


        }

        else
        {

            UiUtils.setError(txtDob,R.string.msg_empty_error);
        }

        if(genderSpinner.getSelectedItemPosition()!=0)
        {

            gender=genderSpinner.getSelectedItemPosition();

        }

        else
        {
           genderSpinner.requestFocus();

        }



        if(!UiUtils.isEmpty(txtPsd))
        {

            password=UiUtils.getText(txtPsd);;

        }
        else
        {

            UiUtils.setError(txtPsd,R.string.msg_empty_error);
        }

        if(!UiUtils.isEmpty(txtRePsd))
        {

            confirm_pswrd=UiUtils.getText(txtRePsd);

            if(confirm_pswrd.equals(password))
            {

            confirm_pswrd=confirm_pswrd;

            }

            else
            {
                UiUtils.setError(txtRePsd,R.string.psdMismatch);
            }

        }

        else
        {

            UiUtils.setError(txtRePsd,R.string.hintRenter);
        }



    }

    private void signUpTask() {
      //  "customerName":"sudeep","address":"pkd","phoneNo":"99951593660","emailId":"sudeeep1235@gmail.com","password":"anju123","zipCode":"673016","profileImage":"anju.jpg","countryCode":"91","regionId":3,"city":1,"country":2,"stateId":2,"customerId":0,"age":23,"dob":"04/03/1995","latitude":"123","longitude":"12121",'gender:"1

        Call<CommonModelList> call = ApiUtils.getRegistrationDetails(first_name,last_name,email,phone,dob,gender,zip_code,password,referalCode);
        call.enqueue(new Callback<CommonModelList>() {
            @Override
            public void onResponse(Call<CommonModelList> call, retrofit2.Response<CommonModelList> response) {
                Log.d("REGISTRATION", String.valueOf(response.code()));
                String err_code = String.valueOf(response.body().getErrorCode());
                String err_msg = String.valueOf(response.body().getErrorMessage());
                if("0".equals(err_code)){


                    //Toast.makeText(getActivity(), "Registration completed successfully", Toast.LENGTH_SHORT).show();
                    sendIntent(getActivity(),MainActivity.class);
                    getActivity().finish();
                }else {
                    Toast.makeText(getActivity(), "Sorry!!Registration failed.Please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonModelList> call, Throwable t) {

                Toast.makeText(getActivity(),"Registration Failed",Toast.LENGTH_LONG).show();

            }
        });
    }
    @Override
    public void SigninWithPhone(PhoneAuthCredential credential) {
        signInWithPhoneAuthCredential(credential);
        dismissDialogue(dialogFragment);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }

    @Override
    public void onStart() {
        super.onStart();
         currentUser = mAuth.getCurrentUser();

    }

    private void initFireBase() {
        mAuth = FirebaseAuth.getInstance();
        verificationStateChangedCallbacks  = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                  @Override
                  public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                      mVerificationInProgress = false;
                      if(smsReciever!=null){
                        getActivity().unregisterReceiver(smsReciever);}

//                  if(!codeSent){
//                      signInWithPhoneAuthCredential(phoneAuthCredential);
//                  }
//                signInWithPhoneAuthCredential(phoneAuthCredential);


                          signInWithPhoneAuthCredential(phoneAuthCredential);
                          dismissDialogue(dialogFragment);
                          showToastAnywhere("Verification Complete");

                  }

                  @Override
                  public void onVerificationFailed(FirebaseException e) {

                      dismissDialogue(dialogFragment);
                      showToastAnywhere("Verification Failed");

                  }

                  @Override
                  public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                      codeSent = true;
                      showToastAnywhere("Verification code has been send on your number");
                      verificationCode = verificationId;
                      mResendToken = token;
                      IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
                      smsReciever=new IncomingSms(verificationCode);
                      getActivity().registerReceiver(smsReciever, filter);
                      dialogFragment=((BaseActivity) getActivity()).openDialogueFragment(OtpDialog.newInstance(SignUp.this,verificationCode));


                  }
              };


    }


    @Override
    public void setTargetFragment(@Nullable Fragment fragment, int requestCode) {
        super.setTargetFragment(fragment, requestCode);

    }

    void handleCaptchaResult(final String responseToken) {
        String url = "https://www.google.com/recaptcha/api/siteverify";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getBoolean("success")){
                                if(!(((Animatable) checkCaptcha.getDrawable()).isRunning()))
                                {
                                    ((Animatable) checkCaptcha.getDrawable()).start();
                                    txtVerification.setText("You're not a Robot");
                                }
                                Toast.makeText(getActivity(),"You're not a Robot",Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception ex) {

                            Log.d(TAG, "Error message: " + ex.getMessage());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if(((Animatable) checkCaptcha.getDrawable()).isRunning())
                        {
                            ((Animatable) checkCaptcha.getDrawable()).stop();
                        }
                        Toast.makeText(getActivity(),"Error message: " + error.getMessage(),Toast.LENGTH_LONG).show();
                        Log.d(TAG, "Error message: " + error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", getString(R.string.CAPTCHA_API_SITE_SECRET_KEY));
                params.put("response", responseToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                80000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

}


