
package com.innoventia.thediscount.net;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.model.CategoryModel;
import com.innoventia.thediscount.model.CommonModel;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.MerchantModel;
import com.innoventia.thediscount.model.LoginModel;

import com.innoventia.thediscount.modules.view.LocationOnMap;
import com.innoventia.thediscount.net.endpoint.ApiService;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ApiUtils {

    //public static final String url = "http://services.hanselandpetal.com/";

   /* public static Retrofit getApiService() {
        return Client.getInstance().returnRetrifit(url);
    }*/

    List<CategoryModel> categoryModels;;

    public static ApiUtils getInstance()
    {
        return new ApiUtils();
    }
    public static JSONObject getJsonObject(String[] keys, String[] values) {
        JSONObject object = new JSONObject();
        for (int i = 0; i < keys.length && i < values.length; i++) {
            try {
                object.put(keys[i], values[i]);
            } catch (Exception e) {
            }
        }
        return object;
    }

    public static HashMap<String, String> getApiParams(String[] keys, String[] values) {
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < keys.length && i < values.length; i++) {
            try {
                map.put(keys[i], values[i]);
            } catch (Exception e) {
            }
        }
        return map;
    }

    public static Call<CommonModelList>getRegistrationDetails(String firstName,String lastName,String emailId,
                                                              String phoneNo,String dob,int gender,String zipCode,
                                                              String password,String referalcode){
           /*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*/
/*
        firstName = "meenu";lastName="anu";phoneNo="7012967341";emailId="meenusss@gmail.com";password="anu123";
        dob="29-05-1994";gender="1";*/
        Map<String, String> reg_params = new HashMap<>();


        reg_params.put("customerName",firstName+""+lastName);
        reg_params.put("firstName",firstName);
        reg_params.put("lastName",lastName);
        reg_params.put("phoneNo",phoneNo);
        reg_params.put("emailId",emailId);
        reg_params.put("password",password);
        reg_params.put("zipCode",zipCode);
        reg_params.put("dateOfBirth",dob);
        reg_params.put("gender",String.valueOf(gender));
        reg_params.put("referalCode",referalcode);
        Call<CommonModelList> val = Client.getApiClient().create(ApiService.class).getRegistrationDetails(reg_params);
        return val;
    }

    public static Call<LoginModel>getLoginDetails(String email, String password){
     /*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*/
        Map<String,String> login_params = new HashMap<>();
        login_params.put("email","anju@gmail.com");
        login_params.put("password","123");
        login_params.put("roleId","51");
        // Log.d("GET_PARAMS",email+ "   "+password);
        Call<LoginModel> val = Client.getApiClient().create(ApiService.class).getLoginDetails(login_params);
        return val;
    }

    public static Call<CommonModelList> postFeedback(String description){
          /*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*/
        Map<String,String> feedback_params = new HashMap<>();
        feedback_params.put("testimonialId","0");
        feedback_params.put("status","1");
        feedback_params.put("userId","0");
        feedback_params.put("description",description);

        Call<CommonModelList> val = Client.getApiClient().create(ApiService.class).postFeedback(feedback_params);
        return val;
    }
    /*public static Call<CommonModelList<TestimonyModel>> getallTestimonials(){
          *//*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*//*
        Map<String,String> testimoni_params = new HashMap<>();
        testimoni_params.put("testimonialId","0");
        testimoni_params.put("status","1");
        testimoni_params.put("userId","0");
        testimoni_params.put("rowPerPage","100");
        testimoni_params.put("currentIndex","0");
        Call<CommonModelList<TestimonyModel>> val = Client.getApiClient().create(ApiService.class).getallTestimonials(testimoni_params);
        return val;
    }
    public static Call<CommonModelList<AppointmentModel>> getAppointments(){
          *//*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*//*
        Map<String,String> appointment_params = new HashMap<>();
        appointment_params.put("status","1");
        appointment_params.put("merchantId","1");
        Call<CommonModelList<AppointmentModel>> val = Client.getApiClient().create(ApiService.class).getAppointments(appointment_params);
        return val;
    }*/


 /*   public static Call<CommonModelList> deleteAppointmet(String appointmentId){
          *//*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*//*
        Map<String,String> delete_appmnt_params = new HashMap<>();
        delete_appmnt_params.put("appointmentId",appointmentId);
        Call<CommonModelList> val = Client.getApiClient().create(ApiService.class).deleteAppointmet(delete_appmnt_params);
        return val;
    }*/

    public static  Call<CommonModelList<MerchantModel>> getHomeLocation()
    {
        Call<CommonModelList<MerchantModel>> call=ApiUtils.getAllMerchants();
        return call;

    }

    public static CommonModelList<MerchantModel> getContactLocation(Fragment parent)
    {
        List<MerchantModel> merchantModel=new ArrayList<MerchantModel>();
        if(!parent.isDetached()) {
            try {

                merchantModel.add((new MerchantModel(parent.getActivity().getString(R.string.contactAddress),String.valueOf(9.992362), String.valueOf(76.302396))));


                LocationOnMap.commonlocationLists =new CommonModelList<MerchantModel>(merchantModel);;


            } catch (Exception e) {
            }
        }

      return new CommonModelList<MerchantModel>(merchantModel) ;


    }

    public static  Call<CommonModelList<MerchantModel>> getMerchantLocation()
    {

        Call<CommonModelList<MerchantModel>> call=ApiUtils.getAllMerchants();
        return call;

    }



    public static void getAllLocations(final Fragment parent,Call<CommonModelList<MerchantModel>> call) {
        Log.d("getlocation","ss");

       call.enqueue(new Callback<CommonModelList<MerchantModel>>() {
           @Override
           public void onResponse(Call<CommonModelList<MerchantModel>> call, Response<CommonModelList<MerchantModel>> response) {

               if(!parent.isDetached()) {
                   try {
                       Log.d("getlocation",String.valueOf(response.code()));

                       LocationOnMap.commonlocationLists =response.body();


                   } catch (Exception e) {
                   }
               }

           }

           @Override
           public void onFailure(Call<CommonModelList<MerchantModel>> call, Throwable t) {



           }
       });
    }


   /*
    public static Call<CommonModelList<WishListModel>> addWishList(String merchantId){
*//*            long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*//*
        Map<String,String> add_wishlist_params = new HashMap<>();
        add_wishlist_params.put("merchantId",merchantId);
        add_wishlist_params.put("status","1");
        add_wishlist_params.put("userId", "0");
        Call<CommonModelList<WishListModel>> val = Client.getApiClient().create(ApiService.class).addWishList(add_wishlist_params);
        return val;
    }
    public static Call<CommonModelList<WishListModel>> removeWishList(String merchantId,String wishListId){
          *//*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*//*
        Map<String,String> remove_wishlist_params = new HashMap<>();
        remove_wishlist_params.put("merchantId",merchantId);
        remove_wishlist_params.put("status","0");
        remove_wishlist_params.put("wishListId",wishListId);
        Call<CommonModelList<WishListModel>> val = Client.getApiClient().create(ApiService.class).removeWishList(remove_wishlist_params);
        return val;
    }
    public static Call<CommonModelList<WishListModel>> getAllWishList(String merchantId,String wishListId){
          *//*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*//*
        Map<String,String> getall_wishlist_params = new HashMap<>();
        getall_wishlist_params.put("merchantId",merchantId);
        getall_wishlist_params.put("status","0");
        getall_wishlist_params.put("wishListId",wishListId);
        getall_wishlist_params.put("userId","0");
        Call<CommonModelList<WishListModel>> val = Client.getApiClient().create(ApiService.class).removeWishList(getall_wishlist_params);
        return val;
    }*/

    public static Call<CommonModelList<MerchantModel>> getAllMerchants() {
      /*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*/
        Map <String,String> parameters = new HashMap<>();
        parameters.put("status","1");
        parameters.put("currentIndex","0");
        parameters.put("rowPerPage","50");

        Call<CommonModelList<MerchantModel>> val = Client.getApiClient().create(ApiService.class).getAllMerchants(parameters);
        return val;
    }

    public static void getAllMerchantsApi(String categoryId) {

      /*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*/
        Map <String,String> parameters = new HashMap<>();
        parameters.put("categoryId",categoryId);
        parameters.put("status","1");
        parameters.put("currentIndex","0");
        parameters.put("rowPerPage","50");
        Call<CommonModelList<MerchantModel>> val = Client.getApiClient().create(ApiService.class).getAllMerchants(parameters);
        val.enqueue(new Callback<CommonModelList<MerchantModel>>() {
            @Override
            public void onResponse(Call<CommonModelList<MerchantModel>> call, Response<CommonModelList<MerchantModel>> response) {
                try {
                    Log.d("getallmerchants_home", String.valueOf(response.code()));

                /*    MerchantListing.merchantModels = response.body().getData();
                    MerchantListing.merchantListing();*/

                    //MerchantListing.newInstance(response.body());

                    //  Log.d("locationModels", String.valueOf(merchantModels));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CommonModelList<MerchantModel>> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAllMerchants();
                    }
                }, 3000);
            }
        });

    }

    public static Call<CommonModelList<MerchantModel>>searchMerchants(String latitude,String longitude,String data){
          /*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*/
          Map<String,String>allmerchants_params = new HashMap<>();
          allmerchants_params.put("latitude",latitude);
          allmerchants_params.put("longitude",longitude);
          allmerchants_params.put("data",data);
        Call<CommonModelList<MerchantModel>> val = Client.getApiClient().create(ApiService.class).searchMerchants(allmerchants_params);
        return val;
    }

    // common for filter for home
    public static Call<CommonModelList<CategoryModel>> getCategory(){
//        long userId = Preferences.getInstance().getUserid();
//        if(userId < 1) {
//            return  null;
//        }
        Call<CommonModelList<CategoryModel>> val = Client.getApiClient().create(ApiService.class).getCategory("N","0","1","0","0");
        return  val;
    }

    public static Call<CommonModelList>makeAppointment(Long merchantId, Long createdDate, Long appoinmentTime){
           /*  long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }*/

        Map<Long, Long> appmnt_params = new HashMap<>();
        appmnt_params.put(merchantId, Long.valueOf(1));
        appmnt_params.put(createdDate,createdDate);
        appmnt_params.put(appoinmentTime,appoinmentTime);
       /* appmnt_params.put("merchantId","1");
        appmnt_params.put("createdDate",createdDate);
        appmnt_params.put("appoinmentTime",appoinmentTime);*/
        Call<CommonModelList> val = Client.getApiClient().create(ApiService.class).makeAppointment(appmnt_params);
        return val;
    }

    public static void loadImage(Context context, String uri, ImageView imageView){
        Picasso.with(context).load(uri).into(imageView);
    }


   /* public static Call<AddProductResponse> updateCart(String productId, int count) {
        long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }
        return Client.getApiClient().create(ApiService.class).updateCart(getApiParams(new String[]{"product_id", "qty", "user_id"},
                new String[]{productId, String.valueOf(count), String.valueOf(userId)}));
    }

    */
/**
     * @param productId blank to get all products
     * @param count     How many items should remove - acc to productId
     * @param removeAll blank to get all products    Or "all" to delete all
     * @return
     *//*

    public static Call<ShoppingCartMainModel> removeFromCart(String productId, int count, boolean removeAll) {
        long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }
        if (removeAll) {
            //to remove all products
            return Client.getApiClient().create(ApiService.class).removeFromCart(getApiParams(new String[]{"product_id", "user_id", "remove"},
                    new String[]{"", String.valueOf(userId), "all"}));
        } else if (count < 1 && !TextUtils.isEmpty(productId)) {
            //to remove product
            return Client.getApiClient().create(ApiService.class).removeFromCart(getApiParams(new String[]{"product_id", "user_id", "remove"},
                    new String[]{productId, String.valueOf(userId), ""}));
        } else if (count > 0) {
            //to update product count
            return Client.getApiClient().create(ApiService.class).removeFromCart(getApiParams(new String[]{"product_id", "qty", "user_id"},
                    new String[]{productId, String.valueOf(count), String.valueOf(userId)}));
        } else {
            //to get all cart products
            return Client.getApiClient().create(ApiService.class).removeFromCart(getApiParams(new String[]{"product_id", "user_id", "remove"},
                    new String[]{"", String.valueOf(userId), ""}));
        }
    }

    */
/**
     * @param notificationType Type of the notification  should be one of ["offers"]
     * @return
     *//*

    public static Call<ShoppingCartMainModel> getNotifications(String notificationType) {
        long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }
        return Client.getApiClient().create(ApiService.class).getNotifications(getApiParams(new String[]{"product"},
                new String[]{notificationType}));
    }

    public static void updateCartCount(Context context) {
        updateCartCount(context,0);
    }
    */
/**
     * to get total added cart items count
     *
     * @param context
     * @param count      This will check on "onFailure()" Method  -> To limit maximum retry count to 5
     *//*

    public static void updateCartCount(Context context, final int count) {
        final Context ctx=context.getApplicationContext();
        long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return;
        }
        Client.getApiClient().create(ApiService.class).getCartCount(getApiParams(new String[]{"user_id"},
                new String[]{String.valueOf(userId)})).enqueue(new Callback<CartCountModel>() {
            @Override
            public void onResponse(Call<CartCountModel> call, Response<CartCountModel> response) {
                try {
                    if (ctx != null)
                        ctx.sendBroadcast(new Intent(ctx.getString(R.string.intent_action_cart_items_count))
                                .putExtra(ctx.getString(R.string.intent_action_current_cart_count), response.body().getCartCount()));
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<CartCountModel> call, Throwable t) {
                try {
                    if(ctx==null || count>4) {
                        return;
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            updateCartCount(ctx,count+1);
                        }
                    },3000);
                } catch (Exception e) {
                }
            }
        });
    }

    public static Call<MyOrdersBean> getMyOrders(int start,int count) {
        long userId = Preferences.getInstance().getUserid();
        if (userId < 1) {
            return null;
        }
        return Client.getApiClient().create(ApiService.class).getMyOrders(getApiParams(new String[]{"user_id", "start", "end"},
                new String[]{String.valueOf(userId),String.valueOf(start),String.valueOf(count)}));
    }

*/
}

