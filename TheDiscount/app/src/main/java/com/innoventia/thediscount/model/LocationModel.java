package com.innoventia.thediscount.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by C9 on 9/28/2017.
 */

public class LocationModel implements Serializable {
    @SerializedName("merchantId")
    String  merchantId;
    @SerializedName("address")
    String address;
    @SerializedName("latitude")
    String lattitude;
    @SerializedName("longitude")
    String longitude;


    public LocationModel(String merchantId, String address) {
        this.merchantId = ""+merchantId;
        this.address = address;
    }
    public String getMerchantid(){
        return merchantId;
    }

    public long getMerchantId() {
        try {
            return Integer.parseInt(lattitude);
        } catch (Exception e) {
        }
        return -1;
    }

    public String getAddress() {
        return address;
    }

    public double getLat() {
        try {
            return Double.parseDouble(lattitude);
        } catch (Exception e) {
        }
        return 0;
    }

    public double getLng() {
        try {
            return Double.parseDouble(longitude);
        } catch (Exception e) {
        }
        return 0;
    }


    @Override
    public String toString() {
        return "LocationModel{" +
                "merchantId='" + merchantId + '\'' +
                ", address='" + address + '\'' +
                ", lattitude='" + lattitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
