package com.innoventia.thediscount.modules.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HorizontalIcons extends BaseFragment implements View.OnClickListener {


    ImageView appoinment,website,whatsapp,location,phone;
    public HorizontalIcons() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.horizontalitems, container, false);
        phone=view.findViewById(R.id.phn);
        whatsapp= view.findViewById(R.id.whatsapp);
        location=view.findViewById(R.id.location);
        appoinment= view.findViewById(R.id.appintment);
        website=view.findViewById(R.id.website);
        appoinment.setOnClickListener(this);
        website.setOnClickListener(this);
        whatsapp.setOnClickListener(this);
        location.setOnClickListener(this);
        phone.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.phn:

                dial();

                break;
            case R.id.whatsapp:

                whatsapp();

                break;
            case R.id.location:

                location();

                break;
            case R.id.appintment:

                appoinment();

                break;
            case R.id.website:

                website();

                break;

        }


    }

    private void website() {



    }

    private void appoinment() {
    }

    private void location() {
    }

    private void whatsapp() {


    }

    private void dial() {



    }
}
