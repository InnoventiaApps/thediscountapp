package com.innoventia.thediscount.modules.app;


import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.utils.uihelper.AppTextView;

import java.io.IOException;
import java.io.InputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUs extends BaseFragment {

    private AppTextView txtAbtusWeb;
    private WebView aboutusWeb;

    public AboutUs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        baseView= inflater.inflate(R.layout.fragment_about_us, container, false);
      //  txtAbtusWeb = (AppTextView) baseView.findViewById(R.id.txtAbtusWeb);
      //  NestedScrollView ns = (NestedScrollView) aboutUs.findViewById(R.id.Ns);
        aboutusWeb = (WebView) baseView.findViewById(R.id.webview);
        aboutusWeb.loadUrl("file:///android_asset/aboutus.html");
        loadToWebView();
        return baseView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }

    private void loadToWebView() {
        AssetManager assetManager = getActivity().getAssets();
        try {
            InputStream input = assetManager.open("aboutus.html");
            byte[] buffer = new byte[input.available()];
            input.read(buffer);
            input.close();
           /* aboutusWeb.loadDataWithBaseURL("file:///android_asset/",
                    new String(buffer), "text/html", "UTF-8", null);*/
            aboutusWeb.loadDataWithBaseURL("file:///android_asset/",
                    getResources().getString(R.string.aboutus), "text/html", "UTF-8", null);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
