
package com.innoventia.thediscount.modules.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.activity.BaseActivity;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.interfaces.OnBindLocationOnMapListListner;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.LocationModel;
import com.innoventia.thediscount.model.MerchantModel;
import com.innoventia.thediscount.utils.Utilities;

import java.util.List;
import java.util.Locale;

import static android.support.constraint.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link LocationOnMap#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocationOnMap extends BaseFragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LocationLists = "locationLists";
    private static final String ARG_PARAM2 = "param2";
    private FragmentActivity mycontext;
    private SupportMapFragment mapFragment;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    MapView mapView;
    private GoogleMap googleMap;
    private Place availblePlace;

    private boolean isCallingLocation = false;

    private static final int PERMISSION_LOCATION = 15;
    private static final int PERMISSION_PICK_LOCATION = 2;
    private static final int REQUEST_LOCATION = 150;
    private static final int REQUEST_DELIVERY_LOCATION = 151;
    private Location currentLocation;
    private static final String FRACTIONAL_FORMAT = "%.4f";
    private static final String ACCURACY_FORMAT = "%.1fm";

    private FusedLocationProviderClient fusedLocationProviderClient;

    private static final int LOCATION_REQUEST_CODE = 101;
    Context context;
    private LocationModel availblelocation;
    List<MerchantModel> locationModels;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    private GoogleApiClient googleApiClient;
    OnBindLocationOnMapListListner listListner;
    public LocationOnMap() {
        // Required empty public constructor
    }
    public static CommonModelList<MerchantModel> commonlocationLists;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param locationLists Parameter 1.

     * @return A new instance of fragment LocationOnMap.
     */
    // TODO: Rename and change types and number of parameters
    public static LocationOnMap newInstance(CommonModelList<MerchantModel> locationLists) {
        LocationOnMap fragment = new LocationOnMap();
        Bundle args = new Bundle();
        args.putSerializable(LocationLists, locationLists);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toast.makeText(getActivity(), "MMMMMMMMMM", Toast.LENGTH_SHORT).show();
        context = this.getActivity();
        //  TAG="LocationOnMap";
        if (getArguments() != null) {

            commonlocationLists= (CommonModelList<MerchantModel>)getArguments().getSerializable(LocationLists);
        }
        try {
            googleApiClient =BaseActivity.addGoogleApiClient(getActivity(), LocationServices.API, this, this);
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
                return;
            }
        } catch (Throwable e) {

            Log.d("HomLOc_Oncreate_exp", e.getMessage().toString());
        }

    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        Log.d("LocationOnMap", childFragment.getClass().getSimpleName());
    }

    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    Toast.makeText(getActivity(), currentLocation.getLatitude() + "  ********  " + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    Log.d("LATITUDE***", String.valueOf(currentLocation.getLatitude()));

                    Log.d("LONGITUDE***", String.valueOf(currentLocation.getLongitude()));

                } else {
                    Toast.makeText(getActivity(), "No Location recorded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_homelocs, container, false);
        mapView = view.findViewById(R.id.mapViewLocations);
        MapsInitializer.initialize(context);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(LocationOnMap.this);
        fetchLastLocation();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;

        Log.d("HomeLocations_OnAttach",getParentFragment().getClass().getSimpleName());
/*
        if(getParentFragment() instanceof OnBindLocationOnMapListListner)
        {
            Log.d("HomeLocations_OnAttach",getParentFragment().getClass().getSimpleName());
            listListner= (OnBindLocationOnMapListListner) getParentFragment();

        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // mListener = null;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {

            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }

            this.googleMap = googleMap;

            if(commonlocationLists!=null&&commonlocationLists.getData().size()!=0)
            {
                locationModels=commonlocationLists.getData();

                setMarkers();

            }


          // setMarker(listListner.getLocationList());

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_LOCATION && grantResults.length == 2 && (grantResults[0] == PackageManager.PERMISSION_GRANTED || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            if (Utilities.hasLocationPermission(getActivity(), PERMISSION_LOCATION, true))
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    if (googleApiClient != null) {

                        googleApiClient.connect();
                    }
                    return;
                }googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if(resultCode== Activity.RESULT_OK) {
                switch (requestCode) {
                    case REQUEST_DELIVERY_LOCATION:availblelocation= (LocationModel) data.getSerializableExtra(Home.EXTRA_LOCATION);
                     /*   txtLocation.setText(availblelocation.getLocation());
                        txtPinOne.setText(availblelocation.getPincode());*/
                        break;
                    case REQUEST_LOCATION:
                        availblePlace = PlacePicker.getPlace(getActivity(), data);
                        // txtLocation.setText(availblePlace.getAddress());
                        break;
                }
            }
        } catch (Exception e) {
        }
        isCallingLocation=false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


   /*
    private void getAllLocations() {
        Log.d("getlocation","ss");
        Call<CommonModelList<LocationModel>> call=ApiUtils.getAllMerchants();
        call.enqueue(new Callback<CommonModelList<LocationModel>>() {
            @Override
            public void onResponse(Call<CommonModelList<LocationModel>> call, Response<CommonModelList<LocationModel>> response) {

                if(!isDetached()) {
                    try {
                        Log.d("getlocation",String.valueOf(response.code()));
                        locationModels=response.body().getData();
                        Log.d("locationModels", String.valueOf(locationModels));
                        setMarkers();
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonModelList<LocationModel>> call, Throwable t) {
                if(!isDetached()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(!isDetached()) {

                                getAllLocations();
                            }
                        }
                    },3000);

                }
            }
        });
    }*/

/*    synchronized void setMarker(CommonModelList<LocationModel> locationList) {
        // if(availblelocation!=null && googleMap!=null)

        Toast.makeText(getActivity(),"SETMARKERS",Toast.LENGTH_LONG).show();
        if(googleMap!=null)

        {
            locationModels=locationList.getData();

            for(LocationModel m:locationModels) {
                try {
                    Log.d("locationModels*********", String.valueOf(locationModels));
                    LatLng latLng=new LatLng(m.getLat(),m.getLng());
                    Log.d("latLng", String.valueOf(latLng));
                    MarkerOptions marker = new MarkerOptions().position(latLng)
                            .title(m.getAddress());
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                    //  marker.icon(BitmapDescriptorFactory
                    //          .defaultMarker(getResources().getColor(R.color.colorAppOrange)));
                    googleMap.addMarker(marker);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                    // googleMap.setMyLocationEnabled(true);
                    CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);
                    googleMap.moveCamera(center);
                    googleMap.animateCamera(zoom);
                    Log.d("latLng", String.valueOf(latLng));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }*/
    synchronized void setMarkers() {
        // if(availblelocation!=null && googleMap!=null)

        Toast.makeText(getActivity(),"SETMARKERS",Toast.LENGTH_LONG).show();
        if(googleMap!=null)

        {
        if(locationModels!=null)
            for(MerchantModel m:locationModels) {
                try {
                    Log.d("locationModels*********", String.valueOf(locationModels));
                    LatLng latLng=new LatLng(m.getLat(),m.getLng());
                    Log.d("latLng", String.valueOf(latLng));
                    MarkerOptions marker = new MarkerOptions().position(latLng)
                            .title(m.getAddress());
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                    //  marker.icon(BitmapDescriptorFactory
                    //          .defaultMarker(getResources().getColor(R.color.colorAppOrange)));
                    googleMap.addMarker(marker);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                    // googleMap.setMyLocationEnabled(true);
                    CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);
                    googleMap.moveCamera(center);
                    googleMap.animateCamera(zoom);
                    Log.d("latLng", String.valueOf(latLng));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    void getLocation() {
        if (Utilities.hasLocationPermission(getActivity(), PERMISSION_PICK_LOCATION, true)) {
            try {
                if(isCallingLocation) {
                    return;
                }
                isCallingLocation=true;
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                getActivity().startActivityFromFragment(this, builder.build(getActivity()), REQUEST_LOCATION);
            } catch (Exception e) {
                isCallingLocation=false;
            }
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        // Permissions ok, we get last location
        fetchLastLocation();;

        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {



    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void startLocationUpdates() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            showToastAnywhere("You need to enable permissions to display location !");
        }

        fusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback,Looper.myLooper());


    }

    void updatePosition(Location location) {
        String latitudeString = createFractionString(location.getLatitude());
        String longitudeString = createFractionString(location.getLongitude());
        String accuracyString = createAccuracyString(location.getAccuracy());
     /*   latitudeValue.setText(latitudeString);
        longitudeValue.setText(longitudeString);
        accuracyValue.setText(accuracyString);*/
    }

    private String createAccuracyString(float accuracy) {
        return String.format(Locale.getDefault(), ACCURACY_FORMAT, accuracy);
    }

    private String createFractionString(double fraction) {
        return String.format(Locale.getDefault(), FRACTIONAL_FORMAT, fraction);
    }

    private LocationCallback locationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            Location lastLocation = locationResult.getLastLocation();
            updatePosition(lastLocation);
        }
    };


}
