package com.innoventia.thediscount.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {

        @SerializedName("userId")
        @Expose
        private int userId;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phoneNo")
        @Expose
        private String phoneNo;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("entryId")
        @Expose
        private int entryId;
        @SerializedName("last_updated_time")
        @Expose
        private int lastUpdatedTime;
        @SerializedName("status")
        @Expose
        private int status;
        @SerializedName("roleId")
        @Expose
        private int roleId;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("emailVerified")
        @Expose
        private int emailVerified;
        @SerializedName("country")
        @Expose
        private int country;
        @SerializedName("city")
        @Expose
        private int city;
        @SerializedName("regionId")
        @Expose
        private int regionId;
        @SerializedName("stateId")
        @Expose
        private int stateId;
        @SerializedName("companyId")
        @Expose
        private int companyId;
        @SerializedName("deviceId")
        @Expose
        private int deviceId;
        @SerializedName("validate")
        @Expose
        private int validate;
        @SerializedName("countryCode")
        @Expose
        private int countryCode;

        /**
         * No args constructor for use in serialization
         *
         */
        public UserDetails() {
        }

        /**
         *
         * @param status
         * @param countryCode
         * @param password
         * @param validate
         * @param emailVerified
         * @param entryId
         * @param country
         * @param city
         * @param phoneNo
         * @param email
         * @param lastUpdatedTime
         * @param token
         * @param stateId
         * @param userId
         * @param companyId
         * @param deviceId
         * @param regionId
         * @param roleId
         */

        public UserDetails(int userId, String email, String phoneNo, String password, int entryId, int lastUpdatedTime, int status, int roleId, String token, int emailVerified, int country, int city, int regionId, int stateId, int companyId, int deviceId, int validate, int countryCode) {
            super();
            this.userId = userId;
            this.email = email;
            this.phoneNo = phoneNo;
            this.password = password;
            this.entryId = entryId;
            this.lastUpdatedTime = lastUpdatedTime;
            this.status = status;
            this.roleId = roleId;
            this.token = token;
            this.emailVerified = emailVerified;
            this.country = country;
            this.city = city;
            this.regionId = regionId;
            this.stateId = stateId;
            this.companyId = companyId;
            this.deviceId = deviceId;
            this.validate = validate;
            this.countryCode = countryCode;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public int getEntryId() {
            return entryId;
        }

        public void setEntryId(int entryId) {
            this.entryId = entryId;
        }

        public int getLastUpdatedTime() {
            return lastUpdatedTime;
        }

        public void setLastUpdatedTime(int lastUpdatedTime) {
            this.lastUpdatedTime = lastUpdatedTime;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getRoleId() {
            return roleId;
        }

        public void setRoleId(int roleId) {
            this.roleId = roleId;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public int getEmailVerified() {
            return emailVerified;
        }

        public void setEmailVerified(int emailVerified) {
            this.emailVerified = emailVerified;
        }

        public int getCountry() {
            return country;
        }

        public void setCountry(int country) {
            this.country = country;
        }

        public int getCity() {
            return city;
        }

        public void setCity(int city) {
            this.city = city;
        }

        public int getRegionId() {
            return regionId;
        }

        public void setRegionId(int regionId) {
            this.regionId = regionId;
        }

        public int getStateId() {
            return stateId;
        }

        public void setStateId(int stateId) {
            this.stateId = stateId;
        }

        public int getCompanyId() {
            return companyId;
        }

        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        public int getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(int deviceId) {
            this.deviceId = deviceId;
        }

        public int getValidate() {
            return validate;
        }

        public void setValidate(int validate) {
            this.validate = validate;
        }

        public int getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(int countryCode) {
            this.countryCode = countryCode;
        }

    }


