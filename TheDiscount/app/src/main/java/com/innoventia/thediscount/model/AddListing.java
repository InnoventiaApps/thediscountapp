package com.innoventia.thediscount.model;

public class AddListing {
    int img;
    String title;
    String banner;

    public AddListing(int img, String title, String banner) {
        this.img = img;
        this.title = title;
        this.banner = banner;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
