package com.innoventia.thediscount.main.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.ArrayRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.interfaces.BaseActvityInterface;
import com.innoventia.thediscount.main.interfaces.BaseInterface;


public abstract class BaseActivity extends AppCompatActivity implements BaseActvityInterface,BaseFragment.LoadDataListener {


    private static final int TASK_ID =12 ;
    private static final int TASK_BUNDLE =21 ;

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                if(getSupportFragmentManager().getBackStackEntryCount()==0)
                {

                   onBackPressed();

                }

            }
        });
      //implement loader

    }

    public void changeFragment(@NonNull Fragment changeFrament, @IdRes int container)

    {
        getSupportFragmentManager().beginTransaction().replace(container,changeFrament).addToBackStack(changeFrament.getClass().getSimpleName()).commit();
    }

    protected void changeFragment( Fragment changeFrament, @IdRes int container,boolean isFirst)
    {
        if (isFirst)
        {
            changeFrament.getChildFragmentManager().beginTransaction().add(container,changeFrament).addToBackStack(changeFrament.getClass().getSimpleName()).commit();
        }
        else
        {
            changeFragment(changeFrament,container);
        }
    }


    @Override
    public void onBackPressed() {
     //   super.onBackPressed();
    }

    public void onLoadFragmentUi() {

  /* getLoaderManager().initLoader(TASK_ID, TASK_BUNDLE, new LoaderManager.LoaderCallbacks<Bitmap>() {
         @Override
         public Loader<Bitmap> onCreateLoader(final int id, final Bundle args) {

             return new AsyncLoader(BaseActivity.this);
         }

         @Override
         public void onLoadFinished(final Loader<Bitmap> loader, final Bitmap result) {
             if (result == null)
                 return;
             //TODO use result
         }

         @Override
         public void onLoaderReset(final Loader<Bitmap> loader) {
         }
     }); */
  }

    @Override
    public void showToastAnywhere(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void showToastAnywhere(int message) {

        showToastAnywhere(getResources().getString(message));

    }

    @Override
    public void showSnackbarAnywhere(String message) {

    }

    @Override
    public void showDialogAnyWhere(String Message) {

    }

    @Override
    public void showProgress(CharSequence msg) {

    }

    @Override
    public void showProgress(int msg) {

    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void sendIntent(@NonNull Context from, @NonNull Class[] to, boolean condition) {

        Class aClass= condition?to[0]: to[1];
        sendIntent(from,aClass);

    }

    @Override
    public void sendIntent(@NonNull Context from, @NonNull Class to) {

        startActivity(new Intent(from,to));

    }



public static GoogleApiClient addGoogleApiClient(Context context, Api api, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener)
{
    return new GoogleApiClient.Builder(context).
            addApi(api).
            addConnectionCallbacks(connectionCallbacks).
            addOnConnectionFailedListener(onConnectionFailedListener).build();
}

public DialogFragment openDialogueFragment(DialogFragment dialogFragment)
{
    FragmentTransaction ft =getSupportFragmentManager().beginTransaction();
    Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
    if (prev != null) {
        ft.remove(prev);
    }
    ft.addToBackStack(null);
    dialogFragment.setCancelable(false);
    dialogFragment.show(ft, "dialog");
    return dialogFragment;
}

}
