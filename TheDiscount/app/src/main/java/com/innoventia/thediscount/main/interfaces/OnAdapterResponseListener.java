package com.innoventia.thediscount.main.interfaces;

import android.view.View;

public interface OnAdapterResponseListener
{

    void onViewClick(View view, int adapterPosition);
    void onPagination();

}
