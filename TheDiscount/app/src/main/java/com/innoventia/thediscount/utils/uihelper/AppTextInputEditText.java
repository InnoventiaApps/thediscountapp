package com.innoventia.thediscount.utils.uihelper;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

import static com.innoventia.thediscount.app.App.setFont;

public class AppTextInputEditText extends TextInputEditText
{

    public AppTextInputEditText(Context context) {
        super(context);
        init(null);
    }

    public AppTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public AppTextInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        setFont(this,attrs);
    }

}
