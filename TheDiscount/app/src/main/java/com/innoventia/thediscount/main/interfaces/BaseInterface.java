package com.innoventia.thediscount.main.interfaces;

import android.app.Dialog;
import android.support.annotation.StringRes;

public interface BaseInterface
{

     void showToastAnywhere(String message);
     void showToastAnywhere(@StringRes int message);
     void showSnackbarAnywhere(String message);
     void showDialogAnyWhere(String Message);
     void showProgress(CharSequence msg);
     void showProgress(@StringRes int msg);
     void dismissProgress();

   /* Dialog showMessage(OnAlertListener listener, int requestId, int title, int msg, int positiveText, int negativeText, boolean cancelable);
    Dialog showMessage(int title, int msg, int positiveText);
    Dialog showMessage(CharSequence title, CharSequence msg, CharSequence positiveText);
    Dialog showMessage(OnAlertListener listener, int requestId, CharSequence title, CharSequence msg, CharSequence positiveText, CharSequence negativeText, boolean cancelable);

*/

}
