package com.innoventia.thediscount.main.mulitithread;

import android.os.Bundle;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.activity.BaseActivity;

public class ProgressBarActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_bar);
    }
}
