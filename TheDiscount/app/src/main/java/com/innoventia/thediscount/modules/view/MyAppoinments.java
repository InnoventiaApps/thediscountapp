package com.innoventia.thediscount.modules.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.interfaces.OnBindLocationOnMapListListner;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.LocationModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyAppoinments extends BaseFragment implements OnBindLocationOnMapListListner {


    public MyAppoinments() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        baseView= inflater.inflate(R.layout.fragment_my_appoinments, container, false);
        return baseView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }

    @Override
    public CommonModelList<LocationModel> getLocationList() {


        return null;
    }
}
