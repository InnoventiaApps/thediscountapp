package com.innoventia.thediscount.utils.uihelper;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import static com.innoventia.thediscount.app.App.setFont;

/**
 * Created by C9 on 9/20/2017.
 */

public class AppButtonView extends AppCompatButton {
    public AppButtonView(Context context) {
        super(context);
        init(null);
    }

    public AppButtonView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public AppButtonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        setFont(this,attrs);
    }
}
