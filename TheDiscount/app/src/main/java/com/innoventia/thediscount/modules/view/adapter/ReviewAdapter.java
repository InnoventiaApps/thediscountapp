package com.innoventia.thediscount.modules.view.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.interfaces.OnAdapterInitialyzeListener;
import com.innoventia.thediscount.model.AddListing;

import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewHolder> {

    Context context;
    List<AddListing> addListingList;
    OnAdapterInitialyzeListener onAdapterListener;
    Uri uri;
    String string_uri;
    // get all adds

    public ReviewAdapter(Context context, List<AddListing> addListingList, OnAdapterInitialyzeListener onAdapterListener) {
        this.context = context;
        this.addListingList = addListingList;
        this.onAdapterListener = onAdapterListener;
        this.string_uri = string_uri;
        this.context=context;
        this.addListingList=addListingList;
        this.onAdapterListener = onAdapterListener;
        if (onAdapterListener != null && (onAdapterListener instanceof Fragment))
            context = ((Fragment) onAdapterListener).getActivity();
        else if (onAdapterListener != null && (onAdapterListener instanceof Context))
            context = (Context) onAdapterListener;
    }

    public ReviewAdapter(Context context) {
        this.context=context;
    }

    @NonNull
    @Override
    public ReviewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.store_review, null);
        ReviewHolder rcv = new ReviewHolder(layoutView);
        Log.d("onCreateViewHolder= ","Horizontal_AdListingHolder");
        return rcv;

    }

    @Override
    public void onBindViewHolder(@NonNull ReviewHolder vertical_AdListingHolder, int i) {





    }

    @Override
    public int getItemCount() {
        return 9;
    }
}

class ReviewHolder extends RecyclerView.ViewHolder
{


    public ReviewHolder(@NonNull View itemView) {
        super(itemView);
    }
}
