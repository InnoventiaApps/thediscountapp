package com.innoventia.thediscount.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C9 on 9/21/2017.
 */

public class UserModel {
    @SerializedName("id")
    long userId;
    @SerializedName("emailId")
    String email;
    @SerializedName("firstName")
    String firstName;
    @SerializedName("lastName")
    String lastName;
    @SerializedName("image")
    String image;
    @SerializedName("phoneNumber")
    String mobile;

   /* @SerializedName("mob_country_code")
    int mobileCountryCode;*/

  /*  @SerializedName("referalCode")
    String referralCode;*/

   /* @SerializedName("address")
    String address;

    @SerializedName("pin")
    String pin;*/

    @SerializedName("gender")
    String gender;

    @SerializedName("dob")
    String dob;

    public long getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return firstName+" "+lastName;
    }

    public String getImage() {
        return image;
    }

    public String getMobile() {
        return mobile;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

 /*   public String getReferralCode() {
        return referralCode;
    }*/

  /*  public String getAddress() {
        return address;
    }

   /* public int getMobileCountryCode() {
        return mobileCountryCode;
    }*/

  /*  public String getPin() {
        return pin;
    }*/



    //Delete this method - only for demo
    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public UserModel(long userId, String email, String firstName, String lastName, String image, String mobile, String gender, String dob) {
        this.userId = userId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.image = image;
        this.mobile = mobile;
        this.gender = gender;
        this.dob = dob;
    }

    public UserModel(String email, String firstName, String lastName, String image, String mobile, String gender, String dob) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.image = image;
        this.mobile = mobile;
        this.gender = gender;
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", image='" + image + '\'' +
                ", mobile='" + mobile + '\'' +
                ", gender='" + gender + '\'' +
                ", dob='" + dob + '\'' +
                '}';
    }

    /*public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }*/

  /*  public void setAddress(String address) {
        this.address = address;
    }*/

   /* public void setMobileCountryCode(int mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }*/

   /* public void setPin(String pin) {
        this.pin = pin;
    }*/


}
