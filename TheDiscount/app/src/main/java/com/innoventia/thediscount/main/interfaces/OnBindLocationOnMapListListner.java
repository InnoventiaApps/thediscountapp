package com.innoventia.thediscount.main.interfaces;

import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.LocationModel;

public interface OnBindLocationOnMapListListner {

    CommonModelList<LocationModel> getLocationList();
}
