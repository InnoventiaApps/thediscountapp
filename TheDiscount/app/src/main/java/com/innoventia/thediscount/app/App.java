package com.innoventia.thediscount.app;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.StringRes;

import android.util.AttributeSet;
import android.widget.TextView;


import com.innoventia.thediscount.R;
import com.innoventia.thediscount.data.Preferences;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by C9 on 9/20/2017.
 */

public class App extends Application {

  //  private static Tracker mTracker;
    private static final String FONT_BASE_PATH="font/";
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
     //   FacebookSdk.sdkInitialize(this.getApplicationContext());
        //getDefaultTracker(this);
     //   mTracker.enableExceptionReporting(true);
     //   mTracker.enableAutoActivityTracking(true);
        Preferences.getInstance(this);
        replaceFont("proximanova-semibold",Typeface.createFromAsset(getAssets(),getCommonFont(this)));
       replaceFont("consola",Typeface.createFromAsset(getAssets(),getCommonFont(this)));
        replaceFont("arial",Typeface.createFromAsset(getAssets(),getCommonFont(this)));

       /* replaceFont("MONOSPACE",Typeface.createFromAsset(getAssets(),getCommonFont(this)));
        replaceFont("SANS_SERIF",Typeface.createFromAsset(getAssets(),getCommonFont(this)));*/
//        JodaTimeAndroid.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        context=base;
        super.attachBaseContext(base);
     //   MultiDex.install(this);
    }

   /* synchronized  public static Tracker getDefaultTracker(Context ctx) {
        try {
            if (mTracker == null) {
                GoogleAnalytics analytics = GoogleAnalytics.getInstance(ctx);
                // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG<br />
                mTracker = analytics.newTracker(R.xml.global_tracker);
            }
        } catch (Exception e) {
        }

      return mTracker;
    }*/

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
   /* public static void trackScreenView(Context ctx,String screenName) {
        try {
            getDefaultTracker(ctx);
            // Set screen name.
            mTracker.setScreenName(screenName);

            // Send a screen view.
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

            GoogleAnalytics.getInstance(ctx).dispatchLocalHits();
        } catch (Exception e) {
        }


    }*/

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
  /*  public static void trackException(Context ctx,Exception e) {
        if (e != null) {
            try {
                getDefaultTracker(ctx);

                mTracker.send(new HitBuilders.ExceptionBuilder()
                        .setDescription(
                                new StandardExceptionParser(ctx, null)
                                        .getDescription(Thread.currentThread().getName(), e))
                        .setFatal(false)
                        .build()
                );
            } catch (Exception e1) {
            }
        }
    }*/

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public static void trackEvent(Context ctx,String category, String action, String label) {
        try {

          //  getDefaultTracker(ctx);

            // Build and send an Event.
         //   mTracker.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
        } catch (Exception e1) {
        }

    }

    protected static void replaceFont(String staticTypefaceFieldName,
                                      final Typeface newTypeface) {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
            Map<String, Typeface> newMap = new HashMap<String, Typeface>();
            newMap.put("proximanova_semibold", newTypeface);
            newMap.put("consola", newTypeface);
            newMap.put("arial", newTypeface);
           /* newMap.put("serif", newTypeface);
            newMap.put("monospace", newTypeface);
            newMap.put("default", newTypeface);*/
            try {
                final Field staticField = Typeface.class
                        .getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                staticField.set(null, newMap);
            } catch (Exception e) {
            }
        } else {
            try {
                final Field staticField = Typeface.class
                        .getDeclaredField(staticTypefaceFieldName);
                staticField.setAccessible(true);
                staticField.set(null, newTypeface);
            } catch (NoSuchFieldException e) {
            } catch (IllegalAccessException e) {
            }
        }
    }

    public static String getFontName(Context ctx, AttributeSet attrs) {
        if (attrs != null) {
            try {
                for (int i = 0, j = attrs.getAttributeCount(); i < j; i++) {
                    if (attrs.getAttributeName(i).equals("font")) {
                        return ctx.getString(attrs.getAttributeResourceValue(i, getCommonFontId()));
                    }
                }
            } catch (Exception e) {
            }
        }
        return ctx.getString(getCommonFontId());
    }

    public static void setFont(TextView view, AttributeSet attrs) {
        try {
            view.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), FONT_BASE_PATH+getFontName(view.getContext(),attrs)));
        } catch (Exception e) {
        }
    }
    public static Typeface getTypeface(Context ctx, @StringRes int fontId) {
        return Typeface.createFromAsset(ctx.getAssets(), FONT_BASE_PATH+ctx.getString(fontId));
    }
    public static Typeface getCommonTypeface(Context ctx) {
        return Typeface.createFromAsset(ctx.getAssets(), getCommonFont(ctx));
    }

    public static String getCommonFont(Context ctx) {
        return FONT_BASE_PATH+ctx.getString(getCommonFontId());
    }
    public static int getCommonFontId() {
        return R.string.font_ariel;
    }
}
