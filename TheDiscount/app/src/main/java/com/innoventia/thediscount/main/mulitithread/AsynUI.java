package com.innoventia.thediscount.main.mulitithread;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;

public class AsynUI extends AsyncTask
{

    OnBackgroundUIListner mListener;
    Context context;


    public AsynUI(OnBackgroundUIListner mListener )
    {
       this.mListener=mListener;

    }
    public AsynUI(Context context,OnBackgroundUIListner mListener )
    {

        this.mListener=mListener;
        this.context=context;

    }

    @Override
    protected void onPreExecute() {
     if(context!=null)
     {
         LayoutInflater inflater = LayoutInflater.from((this.context));
         View view = inflater.inflate(R.layout.progress_bar,null);
         final AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
         builder.setView(view);
     }
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        try {

           mListener.OnLoadBackground();

        }

        catch (Exception re)
        {
            re.printStackTrace();
        }
        return null;
    }





}
