package com.innoventia.thediscount.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.innoventia.thediscount.R;

public class Utilities {

    public static int [] getScreenResolution(Activity act) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return new int [] {displayMetrics.widthPixels, displayMetrics.heightPixels};

    }



    public static boolean hasPermission(Context ctx, int requestCode, String[] permissions, String toastMessage) {
        int permission = 0;
        for (int i = 0; i < permissions.length; i++) {
            permission += ContextCompat.checkSelfPermission(ctx,
                    permissions[i]);
            if (permission != PackageManager.PERMISSION_GRANTED && ctx instanceof Activity) {
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) ctx, permissions[i]);
            }
        }
        if (permission
                != PackageManager.PERMISSION_GRANTED) {
            if (ctx instanceof Activity) {

                ActivityCompat.requestPermissions((Activity) ctx,
                        permissions,
                        requestCode);
            } else if (toastMessage != null)
                Toast.makeText(ctx, toastMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public static boolean hasPermission(Context ctx, int requestCode, String[] permissions, boolean toast) {
        return hasPermission(ctx, requestCode, permissions, toast ? ctx.getString(R.string.msg_need_permission) : null);
    }

    public static boolean hasLocationPermission(Context ctx, int requestCode, boolean toast) {
//        return hasPermission(ctx, requestCode, new String[]{"android.permission.ACCESS_MOCK_LOCATION", Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, toast);
        return hasPermission(ctx, requestCode, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, toast);
    }

    public static boolean isGpsOn(Context ctx) {
        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        return Settings.Secure
//                .isLocationProviderEnabled(ctx.getContentResolver(),
//                        LocationManager.GPS_PROVIDER);
    }

    public static boolean isNetworkConnected(Context ctx) {
        if (!hasPermission(ctx, 101, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, false)) {
            return false;
        }
        ConnectivityManager connectivity = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Network[] networks = connectivity.getAllNetworks();
                if (networks != null) {
                    for (Network network : networks) {
                        if (connectivity.getNetworkInfo(network).getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
                return false;
            }

            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo networkInfo : info)
                    if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }

        return false;
    }




    public static void hideKeyBoard(Activity activity) {
        try {
            hideKeyBoard(activity.getCurrentFocus()==null?activity.findViewById(android.R.id.content):activity.getCurrentFocus());
        } catch (Exception e) {
        }

    }


    public static void hideKeyBoard(View view) {

        if (view != null) {
            // view.clearFocus();
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }




}
