package com.innoventia.thediscount.main.interfaces;

import android.content.Context;
import android.support.annotation.NonNull;

public interface BaseActvityInterface extends BaseInterface {

    void sendIntent(@NonNull Context from, @NonNull Class[] to, boolean condition) ;
    void sendIntent(@NonNull Context from,@NonNull Class to);

}
