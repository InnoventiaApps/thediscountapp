package com.innoventia.thediscount.model;

import java.io.Serializable;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerDetails {

    @SerializedName("customerId")
    @Expose
    private int customerId;
    @SerializedName("createdDate")
    @Expose
    private int createdDate;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("isSpecial")
    @Expose
    private int isSpecial;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("customerNameAr")
    @Expose
    private Object customerNameAr;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("addressAr")
    @Expose
    private Object addressAr;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("phoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("gender")
    @Expose
    private int gender;
    @SerializedName("companyId")
    @Expose
    private int companyId;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;
    @SerializedName("validate")
    @Expose
    private int validate;
    @SerializedName("countryCode")
    @Expose
    private int countryCode;
    @SerializedName("dateOfBirth")
    @Expose
    private Object dateOfBirth;
    @SerializedName("age")
    @Expose
    private int age;
    @SerializedName("walletId")
    @Expose
    private int walletId;
    @SerializedName("walletDetails")
    @Expose
    private Object walletDetails;
    @SerializedName("emailValidate")
    @Expose
    private int emailValidate;
    @SerializedName("phoneValidate")
    @Expose
    private int phoneValidate;
    @SerializedName("customerName")
    @Expose
    private String customerName;


    UserWallet wallet;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param dateOfBirth
     * @param addressAr
     * @param validate
     * @param walletId
     * @param emailId
     * @param customerId
     * @param userId
     * @param zipCode
     * @param age
     * @param gender
     * @param longitude
     * @param createdDate
     * @param deviceId
     * @param customerName
     * @param profileImage
     * @param status
     * @param countryCode
     * @param isSpecial
     * @param customerNameAr
     * @param phoneNo
     * @param emailValidate
     * @param address
     * @param latitude
     * @param companyId
     * @param phoneValidate
     * @param walletDetails
     */

    //String firstName, String lastName, String emailId,
    //                                                              String phoneNo, String dob, String gender, String zipCode,
    //
    //
    //                                                              String password
    public CustomerDetails(int customerId, int createdDate, int status, int isSpecial, String customerName, Object customerNameAr, String address, Object addressAr, int userId, String phoneNo, String deviceId, String emailId, double latitude, double longitude, String profileImage, int gender, int companyId, String zipCode, int validate, int countryCode, Object dateOfBirth, int age, int walletId, Object walletDetails, int emailValidate, int phoneValidate) {
        super();
        this.customerId = customerId;
        this.createdDate = createdDate;
        this.status = status;
        this.isSpecial = isSpecial;
        this.firstName = customerName;
        this.customerNameAr = customerNameAr;
        this.address = address;
        this.addressAr = addressAr;
        this.userId = userId;
        this.phoneNo = phoneNo;
        this.deviceId = deviceId;
        this.emailId = emailId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.profileImage = profileImage;
        this.gender = gender;
        this.companyId = companyId;
        this.zipCode = zipCode;
        this.validate = validate;
        this.countryCode = countryCode;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.walletId = walletId;
        this.walletDetails = walletDetails;
        this.emailValidate = emailValidate;
        this.phoneValidate = phoneValidate;
    }

    public CustomerDetails(String customerName,String firstName, String lastName, String emailId,String phoneNo, String dob, int gender, String zipCode, String password,UserWallet wallet) {
        super();
        this.customerName = customerName;

        this.firstName = firstName;
        this.lastName = lastName;

        this.phoneNo = phoneNo;

        this.emailId = emailId;


        this.gender = gender;

        this.zipCode = zipCode;

        this.dateOfBirth = dob;
        this.wallet= wallet;

    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(int createdDate) {
        this.createdDate = createdDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsSpecial() {
        return isSpecial;
    }

    public void setIsSpecial(int isSpecial) {
        this.isSpecial = isSpecial;
    }

    public String getCustomerName() {
        return firstName;
    }

    public void setCustomerName(String customerName) {
        this.firstName = customerName;
    }

    public Object getCustomerNameAr() {
        return customerNameAr;
    }

    public void setCustomerNameAr(Object customerNameAr) {
        this.customerNameAr = customerNameAr;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getAddressAr() {
        return addressAr;
    }

    public void setAddressAr(Object addressAr) {
        this.addressAr = addressAr;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public int getValidate() {
        return validate;
    }

    public void setValidate(int validate) {
        this.validate = validate;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public Object getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Object dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public Object getWalletDetails() {
        return walletDetails;
    }

    public void setWalletDetails(Object walletDetails) {
        this.walletDetails = walletDetails;
    }

    public int getEmailValidate() {
        return emailValidate;
    }

    public void setEmailValidate(int emailValidate) {
        this.emailValidate = emailValidate;
    }

    public int getPhoneValidate() {
        return phoneValidate;
    }

    public void setPhoneValidate(int phoneValidate) {
        this.phoneValidate = phoneValidate;
    }

}


