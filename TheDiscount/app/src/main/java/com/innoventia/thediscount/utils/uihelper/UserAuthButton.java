package com.innoventia.thediscount.utils.uihelper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.innoventia.thediscount.R;

public class UserAuthButton extends LinearLayout {

    View rootView;
    TextView txtLabel;
    ImageView imgLeft;
    String label;
    Drawable resource;
    //View plusButton;*/
    private static String STATE_SUPER_CLASS = "SuperClass";

    public UserAuthButton(Context context) {
        super(context);
      //  initializeViews(context);
        setOrientation(HORIZONTAL);
        setPadding(5,1,3,1);
        setGravity(Gravity.CENTER);
        setMinimumWidth(LayoutParams.WRAP_CONTENT);
    }

    public UserAuthButton(Context context, AttributeSet attrs) {
        super(context, attrs);

       TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomAppButton);

        try {
            resource = typedArray.getDrawable(R.styleable.CustomAppButton_app_btn_img);
            label = typedArray.getString(R.styleable.CustomAppButton_app_btn_text);

        } finally {
            typedArray.recycle();
        }

       // setWeightSum(3.2f);

        initializeViews(context);
    }

    private void initializeViews(Context context) {

        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)

        rootView = inflate(context, R.layout.userauth_button, this);
        rootView.setBackground(getResources().getDrawable(R.drawable.onselect_userauth));

      //  rootView.setBackgroundColor(getResources().getColor(R.color.colorAppyello));
        txtLabel = (TextView) rootView.findViewById(R.id.txtLabel);
        imgLeft = rootView.findViewById(R.id.fabLeft);
        txtLabel.setText(label);
        imgLeft.setImageDrawable(resource);


    }


}




