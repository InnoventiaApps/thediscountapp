package com.innoventia.thediscount.modules.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.interfaces.OnAdapterInitialyzeListener;
import com.innoventia.thediscount.main.interfaces.OnAdapterResponseListener;
import com.innoventia.thediscount.model.CategoryModel;

import java.util.List;

public abstract class MerchantsAdapter extends RecyclerView.Adapter<MerchantsViewHolder> implements OnAdapterResponseListener {

    Context context;
    List<CategoryModel> categories;
    OnAdapterInitialyzeListener onAdapterListener;
    public MerchantsAdapter(Context context, List<CategoryModel> categories, OnAdapterInitialyzeListener onAdapterListener) {

        this.context=context;
        this.categories=categories;
        this.onAdapterListener = onAdapterListener;
        if (onAdapterListener != null && (onAdapterListener instanceof Fragment))
            context = ((Fragment) onAdapterListener).getActivity();
        else if (onAdapterListener != null && (onAdapterListener instanceof Context))
            context = (Context) onAdapterListener;
    }



    public void clear() {
        onAdapterListener = null;
        context = null;
        categories.clear();
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MerchantsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.merchant_item, null);
        MerchantsViewHolder rcv = new MerchantsViewHolder(layoutView);

        return rcv;


    }

    @Override
    public void onBindViewHolder(@NonNull MerchantsViewHolder merchantsViewHolder, int i) {

        merchantsViewHolder.categoryName.setText("");
       // homeCategoryViewHolder.categoryIcon.setImageURI("");


    }

    @Override
    public int getItemCount() {
        return 0;
    }

/*    @Override
    public void initialise() {

        MakeListFragment.adapter=this;
        MakeListFragment.onAdapterResponseListener=this;
      // MakeListFragment.list=(List<Object>)categories;
    }*/

    @Override
    public void onPagination() {

    }


}

class MerchantsViewHolder extends RecyclerView.ViewHolder
{
    TextView categoryName;
    ImageView categoryIcon;
    private boolean isCallingNewList = false;

    public MerchantsViewHolder(@NonNull View itemView) {
        super(itemView);
         categoryName=itemView.findViewById(R.id.tvCategoryLabel);
         categoryIcon=itemView.findViewById(R.id.imagCategoryIcon);

         //if (onAdapterListener != null)
        //                                onAdapterListener.onViewClick(null, -1);
    }

/*
    private synchronized void getNewList() {
        try {
            if (isCallingNewList) {
                return;
            }
            final String preSortType = sortType;
            isCallingNewList = true;
            progress.setVisibility(View.VISIBLE);


//            if (type.equals("offers")) {
//                call = Client.getApiClient().create(ApiService.class).getOffers(ApiUtils.getApiParams(new String[]{"product", "start", "end"},
//                        new String[]{type, needToClearList?"0":String.valueOf(newListAdapter.getItemCount()), loadingCount}));
//            } else {
            isCallingNewList=false;
        } catch (Exception e) {
        }

    }*/




}
