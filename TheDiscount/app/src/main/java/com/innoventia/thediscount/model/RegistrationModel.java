package com.innoventia.thediscount.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegistrationModel {


        @SerializedName("Details")
        @Expose
        private UserDetails details;

        @SerializedName("login")
        @Expose
        private UserWallet wallet;

        @SerializedName("CustomerDetails")
        @Expose
        private CustomerDetails customerDetails;

        /**
         * No args constructor for use in serialization
         *
         */
        public RegistrationModel() {
        }

        /**
         *
         * @param wallet
         * @param customerDetails
         * @param login
         */
   /*     public RegistrationModel(UserWallet wallet, Details login, UserDetails customerDetails) {
            super();
            this.details = details;
            this.wallet = login;
            this.customerDetails = customerDetails;
        }

        public Details getDetails() {
            return details;
        }

        public void setDetails(Details details) {
            this.details = details;
        }

        public Login getLogin() {
            return login;
        }

        public void setLogin(Login login) {
            this.login = login;
        }

        public CustomerDetails getCustomerDetails() {
            return customerDetails;
        }

        public void setCustomerDetails(CustomerDetails customerDetails) {
            this.customerDetails = customerDetails;
        }

    }*/


}
