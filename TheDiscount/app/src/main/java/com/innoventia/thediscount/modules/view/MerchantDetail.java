package com.innoventia.thediscount.modules.view;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.fragment.MakeListFragment;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.MerchantModel;
import com.innoventia.thediscount.modules.view.adapter.Horizontal_AdListingAdapter;
import com.innoventia.thediscount.utils.UiUtils;
import com.innoventia.thediscount.utils.uihelper.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MerchantDetail#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MerchantDetail extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    com.daimajia.slider.library.SliderLayout offer_ViewPager;
    com.daimajia.slider.library.Indicators.PagerIndicator mIndicator;
    ViewPager viewPager;   TabLayout tabs;
    private RecyclerView recyler;
    List<MerchantModel> merchantModels;
    public MerchantDetail() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment MerchantDetail.
     */
    // TODO: Rename and change types and number of parameters
    public static MerchantDetail newInstance(CommonModelList<MerchantModel> merchantModelCommonModelList) {
        MerchantDetail fragment = new MerchantDetail();
        Bundle args = new Bundle();
      //  args.putString(MerchantListing.mer, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

           // merchantModels = getArguments().getString(comm);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        baseView=inflater.inflate(R.layout.fragment_merchant_detail, container, false);

        offer_ViewPager=baseView.findViewById(R.id.offer_view_pager);
         mIndicator=baseView.findViewById(R.id.custom_indicator);;
            recyler=baseView.findViewById(R.id.list);

            setViewPager();
            setRecyler();
        // Inflate the layout for this fragment

        return baseView;
    }

    private void setRecyler() {


        GridLayoutManager lLayout = new GridLayoutManager(getActivity(), 3);
        recyler.setHasFixedSize(true);
        int valueInPixels = (int) getResources().getDimension(R.dimen.paddingXSmall);
        recyler.addItemDecoration(new GridSpacingItemDecoration(3, UiUtils.dpToPx(context, valueInPixels), true));
        lLayout = new GridLayoutManager(getActivity(),3);
        recyler.setLayoutManager(lLayout);
        recyler.setAdapter(new Horizontal_AdListingAdapter(getActivity()));

    }

    private void setViewPager() {
        viewPager = (ViewPager)baseView.findViewById(R.id.viewpager);
        setupWithViewPager(viewPager);

        tabs = (TabLayout)baseView.findViewById(R.id.sliding_tabs);
        tabs.setupWithViewPager(viewPager);

        if(viewPager.getAdapter()!=null)
        {
            createTabIcons(viewPager);

        }

        viewPager.setCurrentItem(0);

    }

    private void setupWithViewPager(ViewPager viewPager) {

        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new DiscountList(), "DiscountList");
        adapter.addFragment(new Review(), "Review");
        adapter.addFragment(new Description(),"Description");
        viewPager.setAdapter(adapter);
    }


    private void createTabIcons(ViewPager viewPager) {

       int count=viewPager.getAdapter().getCount();

        for(int i=0;i<count;i++)
        {
            TextView tab = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab, null);
            tab.setText(viewPager.getAdapter().getPageTitle(i));
            tabs.getTabAt(i).setCustomView(tab);
        }

    }

    static class Adapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> tiltleList = new ArrayList<>();


        private Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            tiltleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tiltleList.get(position);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }

    private void setOfferImageSlider_onResume() {

        try {
            int[] imagesslide = new int[]{R.drawable.logo_1920_1290, R.drawable.logo_1920_1290};
            for (int i : imagesslide) {

                TextSliderView textSliderView = new TextSliderView(getContext());
                // initialize a SliderLayout
                textSliderView
                        .image(i)
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {

                                //do code

                            }
                        });

                //add your extra information
                textSliderView.bundle(new Bundle());
                textSliderView.getBundle()
                        .putString("extra", "" + i);


                //edited render_type.xml of library1.1.5 to hide layout
                /*   offer_ViewPager.getPagerIndicator().setLayoutParams(new RelativeLayout.LayoutParams(5,5));*/

                offer_ViewPager.setPresetTransformer(SliderLayout.Transformer.Accordion);
                offer_ViewPager.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                offer_ViewPager.addSlider(textSliderView);
            }

            // offer_ViewPager.getAnimation().
            offer_ViewPager.setDuration(4000);

            offer_ViewPager.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            ;
        }
    }



}
