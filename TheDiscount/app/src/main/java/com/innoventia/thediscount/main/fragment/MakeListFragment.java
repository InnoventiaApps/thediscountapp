package com.innoventia.thediscount.main.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.data.Preferences;
import com.innoventia.thediscount.main.interfaces.OnAdapterResponseListener;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.main.mulitithread.AsynUI;
import com.innoventia.thediscount.modules.view.adapter.GenericRecyclerAdapter;
import com.innoventia.thediscount.modules.view.adapter.GenericViewHolder;
import com.innoventia.thediscount.utils.UiUtils;
import com.innoventia.thediscount.utils.Utilities;
import com.innoventia.thediscount.utils.uihelper.BottomNavigationBehavior;
import com.innoventia.thediscount.utils.uihelper.GridSpacingItemDecoration;

import java.util.List;


public abstract class MakeListFragment<T, VH extends GenericViewHolder> extends BaseFragment implements OnBackgroundUIListner ,OnAdapterResponseListener {

    RecyclerView recyclerView; //recyclerView;
    View progress;
    View sort;
    View txtSort;
    TextView txtItemCount;
    View layoutFragList;

    BottomNavigationView navBottom;
    private RecyclerView.LayoutManager lLayout;
    // private ListFragmentAdapter newListAdapter;
    private RecyclerView.OnScrollListener onScrollListener;
    private PopupWindow mPopupWindow;
    // private NonScrollListView nonscrolllist;

    public  OnAdapterResponseListener onAdapterResponseListener;
    public  List<Object> list = null;
    private boolean isCallingNewList = false;
    /**
     * It will use to set value on {@link #txtItemCount} -> On Backstack
     */
    private int totalItemCount = 0;

    boolean isBottomMenuShowing = true;

    String type = "new";
    int loadingCount ;
    int spanCount;

    boolean isNeedPagination = true;
    boolean isNeedSortOption = false;
    boolean needBottomNavigation = false;
    Context context;
    public static android.support.v4.widget.SwipeRefreshLayout swipeRefreshLayout;
  //  OnAdapterInitialyzeListener onAdapterInitialyzeListener;
    private String sortType = "relevant";

    /**
     * Set this as true if sortType is changed
     */
    private boolean needToClearList = false;

    /**
     * Should cancel previous call if sortType is changed
     */
    // private Call<ProductListBean> call;


    private GenericRecyclerAdapter genericRecyclerAdapter;

      //  private NonScrollListView nonscrolllist;
    private ArrayAdapter arrayAdapter;

    private final Handler handler = new Handler();


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {

                getNewList();


            } catch (Exception e) {
            }
        }
    };


public View getAdapterView(Context context)
{
    View layoutView=null;
    if(getLayout()!=0) {
         layoutView = LayoutInflater.from(context).inflate(getLayout(), null);
    }
    return layoutView;
}

    /**
     * @param loadingCount         Items count to load on single webservice
     * @param spanCount            Columncount
     * @param isNeedSortOption     set true if need Sort option at bottom
     * @param isNeedPagination     set false if not need pagination
     * @param clearPrevious        set false if no need to clear previous list(onBackstack on childfragments)
    // * @param needBottomNavigation To show / hide common bottom menu
     */
   /* public static Bundle addBundle(@LayoutRes int layout,int loadingCount, int spanCount, boolean isNeedSortOption, boolean isNeedPagination, boolean clearPrevious) {

        Bundle args = new Bundle();
        args.putInt("layout", layout);
        args.putString("loadingCount", String.valueOf(loadingCount));
        args.putInt("spanCount", spanCount);
        args.putBoolean("isNeedSortOption", isNeedSortOption);
        args.putBoolean("isNeedPagination", isNeedPagination);
        if (clearPrevious)
            Preferences.getInstance().clearList(layout);
        return args;
    }*/

    public static Bundle addBundle(Context context,int loadingCount, int spanCount, boolean isNeedSortOption, boolean isNeedPagination, boolean clearPrevious) {

        Bundle args = new Bundle();

        args.putInt("loadingCount",loadingCount);
        args.putInt("spanCount", spanCount);
        args.putBoolean("isNeedSortOption", isNeedSortOption);
        args.putBoolean("isNeedPagination", isNeedPagination);
       // args.putBoolean("needBottomNavigation", needBottomNavigation);
        if (clearPrevious)
            Preferences.getInstance(context).clearList(R.layout.recycler);
        return args;
    }

    /*  // TODO: Rename and change types and number of parameters
    public static MerchantList newInstance(String param1, String param2) {
        MerchantList fragment = new MerchantList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
*/

    public static MakeListFragment newInstance(Fragment instance, Bundle args){
        MakeListFragment makeListFragment=(MakeListFragment) instance;
        instance.setArguments(args);
        return makeListFragment;
    }
    public abstract List<T> loadList();
    public abstract int getLayout();
    public abstract GenericViewHolder<T> getViewHolder(View childView);



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


         View baseView = inflater.inflate(R.layout.recycler, container, false);
        recyclerView = baseView.findViewById(R.id.list);
        swipeRefreshLayout=(SwipeRefreshLayout)baseView.findViewById(R.id.pullToRefresh);

       //  View view = onAdapterInitialyzeListener.getFragmentView(inflater,container,savedInstanceState);



        // baseView= inflater.inflate(layout, container, false);

       //  recyclerView = baseView.findViewById(R.id.list);

        //getActivity().registerReceiver(mReceiver, new IntentFilter(getString(R.string.intent_action_sort_type_changed)));

        init();
//        sort.setVisibility(View.GONE);

        ///setListeners();


        new AsynUI(this).execute(getActivity());


        /* View v = inflater.inflate(R.layout.frag_product_list, container, false);

        getActivity().registerReceiver(mReceiver, new IntentFilter(getString(R.string.intent_action_sort_type_changed)));
        init();
        sort.setVisibility(View.GONE);
        setListeners();

        setEmptyText(getString(R.string.info_empty_list), getString(R.string.info_items_listed_here));
*/

        return baseView;
    }

    private void init() {

        Bundle b = getArguments();

        if (b != null) {
            // onAdapterInitialyzeListener =(OnAdapterInitialyzeListener) b.getSerializable("onAdapterInitialyzeListener");

            loadingCount = b.getInt("loadingCount");
            spanCount = b.getInt("spanCount");
            isNeedSortOption = b.getBoolean("isNeedSortOption");
            isNeedPagination = b.getBoolean("isNeedPagination");
           // needBottomNavigation = b.getBoolean("needBottomNavigation");

        }

    }


    private synchronized void getNewList() {
        try {
            if (isCallingNewList) {
                return;
            }
            final String preSortType = sortType;
            isCallingNewList = true;
            progress.setVisibility(View.VISIBLE);

            loadList();

//            if (type.equals("offers")) {
//                call = Client.getApiClient().create(ApiService.class).getOffers(ApiUtils.getApiParams(new String[]{"product", "start", "end"},
//                        new String[]{type, needToClearList?"0":String.valueOf(newListAdapter.getItemCount()), loadingCount}));
//            } else {
            isCallingNewList=false;
        } catch (Exception e) {
        }

    }



/*
    private void setItemList() {
        try {

            lLayout = new GridLayoutManager(getActivity(), spanCount);
            recyclerView.setHasFixedSize(true);

            recyclerView.setLayoutManager(lLayout);
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, UiUtils.dpToPx(context,5), true));


          *//*  newListAdapter = new GenericRecyclerAdapter(layout);

            if (spanCount > 1) {
                int horizontal = spanCount > 1 ? UiUtils.dpToPx(5, getResources()) : UiUtils.dpToPx(10, getResources());
                int vertical = spanCount > 1 ? UiUtils.dpToPx(4, getResources()) : UiUtils.dpToPx(8, getResources());
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, horizontal, vertical, horizontal, vertical, true));
            }
*//*
           // newListAdapter.addList(Preferences.getInstance().getProductList(layout, type), false);

            recyclerView.setAdapter(newListAdapter);
            recyclerView.setNestedScrollingEnabled(true);

           // checkParentSortType();

            if (newListAdapter.getItemCount() == 0)
                handler.post(runnable);
            else {
              *//*  txtItemCount.setText(totalItemCount + " " + getString(R.string.items));
                txtItemCount.setVisibility(View.VISIBLE);*//*
                sort.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
        }
    }*/


    private void setItemList() {

        try {

             //lLayout= spanCount>1?new GridLayoutManager(getActivity(), spanCount):new LinearLayoutManager(getActivity());

            if (spanCount > 1) {
                lLayout = new GridLayoutManager(getActivity(), spanCount);
                recyclerView.setHasFixedSize(true);
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, UiUtils.dpToPx(context, 10), true));
                lLayout = new GridLayoutManager(getActivity(),3);


            } else {
                lLayout = new LinearLayoutManager(getActivity());

            }

            recyclerView.setLayoutManager(lLayout);

           View childView= getAdapterView(getActivity());

           genericRecyclerAdapter = new GenericRecyclerAdapter(getViewHolder(childView),loadList());

            //    newListAdapter.addList(Preferences.getInstance().getProductList(layout, type), false);
            recyclerView.setAdapter(genericRecyclerAdapter);
            recyclerView.setNestedScrollingEnabled(true);

            if (genericRecyclerAdapter instanceof OnAdapterResponseListener) {
                onAdapterResponseListener = (OnAdapterResponseListener) genericRecyclerAdapter;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnAdapterResponseListener");
            }

            //checkParentSortType();

            if (genericRecyclerAdapter.getItemCount() == 0)
                handler.post(runnable);
            else {
               /* txtItemCount.setText(totalItemCount + " " + getString(R.string.items));
                txtItemCount.setVisibility(View.VISIBLE);
                sort.setVisibility(View.VISIBLE);*/
            }

        } catch (Exception e) {
        }

    }


    private void setListeners() {

        if (isNeedPagination || needBottomNavigation) {
            onScrollListener = new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    isNeedToCallWeb();
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (needBottomNavigation) {
                        if (dy < -2) {

                            new BottomNavigationBehavior().showHideBottomNavigation(navBottom);
                            // showHideBottomNavigation(true);
                        } else if (dy > 2) {
                            new BottomNavigationBehavior().showHideBottomNavigation(navBottom);
                            ;
                        }
                    }
                    isNeedToCallWeb();
                }
            };
            recyclerView.addOnScrollListener(onScrollListener);
        }


    }

    private void isNeedToCallWeb() {


        if (!isNeedPagination) {
            return;
        }
        onLayoutClick();
        if (isCallingNewList || lLayout == null || genericRecyclerAdapter == null) {
            return;
        }


        //check recyclerview scroll end
        if (lLayout instanceof GridLayoutManager) {

            GridLayoutManager glm = (GridLayoutManager) lLayout;
            if (glm.findLastCompletelyVisibleItemPosition() == genericRecyclerAdapter.getItemCount() - 1) {
//            getNewList();
                handler.post(runnable);
            }
        }
    }


    @Override
    public void onDestroyView() {

        try {
            if (mPopupWindow != null && mPopupWindow.isShowing()) {
                Log.e("Pop", "=" + (mPopupWindow.isShowing()));
                mPopupWindow.dismiss();
            }
            try {
                //   getActivity().unregisterReceiver(mReceiver);
                handler.removeCallbacks(runnable);
            } catch (Exception e) {
            }

            if (recyclerView != null) {
                recyclerView.removeOnScrollListener(onScrollListener);
            }
            if (genericRecyclerAdapter != null) {
                genericRecyclerAdapter = null;
            }
            if (list != null) {
                list = null;
            }

            if (onAdapterResponseListener != null) {
                onAdapterResponseListener = null;
            }

            baseView=null;

        } catch (Exception e) {
        }
     //   layout=0;
        super.onDestroyView();
    }


    void onLayoutClick() {
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }
    }


    public void sort() {
        Utilities.hideKeyBoard(getActivity());


        // Inflate the custom layout/view
        View customView = getActivity().getLayoutInflater().inflate(R.layout.filters, null);

        if (mPopupWindow == null) {
            // Initialize a new instance of popup window
            mPopupWindow = new PopupWindow(
                    customView,
                    CoordinatorLayout.LayoutParams.WRAP_CONTENT,
                    CoordinatorLayout.LayoutParams.WRAP_CONTENT
            );
         //   mPopupWindow.setAnimationStyle(R.style.Dialog_Animation);

        //    arrayAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.sort, R.layout.layout_sort_txt);

            /*nonscrolllist = (NonScrollListView) customView.findViewById(R.id.sortdlg);
            nonscrolllist.setAdapter(arrayAdapter);

            nonscrolllist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (!sortType.equals(getResources().getStringArray(R.array.sortKeys)[position])) {
                        sortTypeChanged(getResources().getStringArray(R.array.sortKeys)[position]);
                    }
                    mPopupWindow.dismiss();
                }
            });

            // Set an elevation value for popup window
            // Call requires API level 21
            if (Build.VERSION.SDK_INT >= 21) {
                mPopupWindow.setElevation(5.0f);
            }
        } else if (mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
            return;
        }

        mPopupWindow.showAsDropDown(txtSort, 0, 0);


    }


    /*private void init() {

        Bundle b = getArguments();

        if (b != null) {
            type = b.getString("type");
            loadingCount = b.getString("loadingCount");
            spanCount = b.getInt("spanCount");
            isNeedSortOption = b.getBoolean("isNeedSortOption");
            isNeedPagination = b.getBoolean("isNeedPagination");
            //  needBottomNavigation = b.getBoolean("needBottomNavigation");
            layout = getArguments().getInt("layout");

            /*title = b.getString("title");
            if (title != null)
                getActivity().setTitle(b.getString("title"));*/
        }


        sort.setVisibility(!isNeedSortOption ? View.GONE : View.VISIBLE);

        /**
         * Changing layout gravity from bottom to center If this is a child of any fragment
         */
        if (getParentFragment() != null) {
//            setGravityOfProgressView(Gravity.CENTER);
        }


        /**
         * Toggle layout gravity from bottom to center|top
         * Condition ->     newListAdapter.getItemCount()==0?Gravity.CENTER:Gravity.BOTTOM;
         * Condition2 ->
         */
    /*    private void setGravityOfProgressView(int gravity){
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) progress.getLayoutParams();
            lp.gravity = gravity;
            progress.setLayoutParams(lp);

        }*/


        /**
         * Adding scroll listener to recyclerview for pagination
         */

    }

    private void checkIsListEmpty() {
        boolean isEmpty = genericRecyclerAdapter.getItemCount() == 0;
        // changeEmptyVisibility(isEmpty && isNeedSortOption);
        sort.setVisibility(isEmpty || !isNeedSortOption ? View.GONE : View.VISIBLE);
        sort.setVisibility(!isNeedSortOption ? View.GONE : View.VISIBLE);
    }

        private void sortTypeChanged (String sortType){
           /* if (call != null)
                call.cancel();*/
            this.sortType = sortType;
          /*  if (getParentFragment() != null && getParentFragment() instanceof HomeFragment) {
                ((HomeFragment) getParentFragment()).setSortType(sortType);
            }*/
            isCallingNewList = false;
            needToClearList = true;
            handler.removeCallbacks(runnable);

            handler.post(runnable);
        }

       private void checkParentSortType () {
            try {
               // if (getParentFragment() != null && getParentFragment() instanceof Home) {
                  //  String type = ((Home) getParentFragment()).getSortType();
                    if (!this.sortType.equals(type)) {
                        sortTypeChanged(type);
                    }
               // }
            } catch (Exception e) {
            }
        }

    /*    private synchronized void getNewList()
        {
            try {
                if (isCallingNewList) {
                    return;
                }
                final String preSortType = sortType;
                isCallingNewList = true;
                progress.setVisibility(View.VISIBLE);


//            if (type.equals("offers")) {
//                call = Client.getApiClient().create(ApiService.class).getOffers(ApiUtils.getApiParams(new String[]{"product", "start", "end"},
//                        new String[]{type, needToClearList?"0":String.valueOf(newListAdapter.getItemCount()), loadingCount}));
//            } else {



                call = Client.getApiClient().create(ApiService.class).getProducts(ApiUtils.getApiParams(new String[]{"product", "start", "end", "order_by"},
                        new String[]{type, needToClearList ? "0" : String.valueOf(newListAdapter.getItemCount()), loadingCount, sortType}));
//            }
                call.enqueue(new Callback<ProductListBean>() {
                    @Override
                    public void onResponse(Call<ProductListBean> call, Response<ProductListBean> response) {
                        try {
                            progress.setVisibility(View.GONE);

                            newListAdapter.addList(response.body().getProducts(), needToClearList);
                            txtItemCount.setText(response.body().getCount() + " " + getString(R.string.items));
                            totalItemCount = response.body().getCount();
                            txtItemCount.setVisibility(View.VISIBLE);
                            needToClearList = false;
                            //  Preferences.getInstance().addProductList(newListAdapter.getItemList(), layout, type);

                            *//*if (newListAdapter.getItemCount() > 0 && !((HomeActivity) getActivity()).isAppUpdateChecked()) {
                                Preferences.getInstance().setUpdate(response.body().getVersionCode(), response.body().getIsUpdateNecessary() == 1);
                                ((HomeActivity) getActivity()).showUpdateDilaog();
                            }*//*
                            //  checkIsListEmpty();
                        } catch (Exception e) {
                        }
                        isCallingNewList = false;
                    }

                    @Override
                    public void onFailure(Call<ProductListBean> call, Throwable t) {
                        try {
                            if (preSortType.equals(sortType))
                                progress.setVisibility(View.GONE);
                            if (!needToClearList) {

                                // checkIsListEmpty();
                            }
                        } catch (Exception e) {
                        }
                        isCallingNewList = false;
                    }
                });
            } catch (Exception e) {
            }

        }*/

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isDetached()) {
                try {
                     checkParentSortType();
                } catch (Exception e) {
                }
            }
        }
    };


    public void setAdapterValue(GenericRecyclerAdapter adapter) {

        this.genericRecyclerAdapter = adapter;

    }

    public void setListValue(List<Object> list) {
        this.list = list;
    }

    @Override
    public void OnLoadBackground() {

        setItemList();

    }

   /* public interface OnAdapterValuesSetLisnter
    {

    public void onSetAdapterValues(RecyclerView.Adapter a, List<Object> list) ;

      //  adapter=new HomeCategoryAdapter(null,null);


    }*/


    private synchronized void makeNewList() {
        try {
            if (isCallingNewList)
                return;

            final String preSortType = sortType;
            isCallingNewList = true;
            progress.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);


            checkIsListEmpty();
        }

            catch (Exception e) {
            }
            isCallingNewList = false;


    }

/*    @Override
    public void initialise(RecyclerView.Adapter adapter ,
          OnAdapterResponseListener onAdapterResponseListener,
              List<Object> list) {


       this.adapter=adapter;
       this.onAdapterResponseListener=onAdapterResponseListener;
       this.list=list;

    }*/

    @Override
    public void onViewClick(View view, int adapterPosition) {

        try {
            if (adapterPosition <0 && getActivity() != null) {
                checkIsListEmpty();

               // txtTotalAmount.setText(String.valueOf(adapter.getTotalAmount()));
              //  txtCartCountMsg.setText(String.format("You have added %d item%s", adapter.getItemCount(), adapter.getItemCount() == 1 ? "" : "s"));
                if(genericRecyclerAdapter.getItemCount()<2) {
                    CoordinatorLayout.LayoutParams coordinatorLayoutParams = (CoordinatorLayout.LayoutParams) navBottom.getLayoutParams();
                    coordinatorLayoutParams.setBehavior(null);
                }
            }
        } catch (Exception e) {
        }

    }

    @Override
    public void onPagination() {


    }


    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        this.context=context;

    }

}



           /*     @Override
                public void onFailure(Call<ProductListBean> call, Throwable t) {
                    try {
                        if (preSortType.equals(sortType))
                            progress.setVisibility(View.GONE);
                        if (!needToClearList) {

                            checkIsListEmpty();
                        }
                    } catch (Exception e) {
                    }
                    isCallingNewList = false;
                }
            });
        } catch (Exception e) {
        }
}}
*/








