package com.innoventia.thediscount.main.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.main.mulitithread.AsynUI;
import com.innoventia.thediscount.modules.app.AboutUs;
import com.innoventia.thediscount.modules.app.ContactUs;
import com.innoventia.thediscount.modules.app.Refer;
import com.innoventia.thediscount.modules.app.Testimony;
import com.innoventia.thediscount.modules.view.Home;
import com.innoventia.thediscount.modules.view.MyAppoinments;
import com.innoventia.thediscount.modules.view.MyDiscount;
import com.innoventia.thediscount.modules.view.MyFavourite;
import com.innoventia.thediscount.utils.Utilities;
import com.innoventia.thediscount.utils.uihelper.AppTextView;

import java.util.ArrayList;

public class MainActivity extends BaseActivity
          implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, NavigationView.OnNavigationItemSelectedListener , DrawerLayout.DrawerListener, BottomNavigationView.OnNavigationItemSelectedListener,OnBackgroundUIListner {

    private static int container=R.id.main_content;
    private Location location1;
 //   private TextView locationTv;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    // lists for permissions
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;
    private static final int PERMISSION_LOCATION = 15;
    NavigationView navigationView;
    public  AppTextView txtTitle;
    public static android.support.design.widget.BottomNavigationView bottomNavigationView;
    public FusedLocationProviderClient fusedLocationProviderClient;
    public static final int LOCATION_REQUEST_CODE = 101;
    private Location currentLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtTitle=toolbar.findViewById(R.id.title);


        FloatingActionButton fab = findViewById(R.id.fab);
      /*  fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
         navigationView = findViewById(R.id.nav_view);
        bottomNavigationView= findViewById(R.id.bottom);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        //googleApiClient= BaseActivity.addGoogleApiClient(this,LocationServices.API,this,this);
      //  new AsynUI(this).execute();
        // we build google api client
       // addGoogleApiClient(MainActivity.this,LocationServices.API,this,this);

        try {
            googleApiClient =BaseActivity.addGoogleApiClient(MainActivity.this, LocationServices.API, this, this);
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);
            if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
                return;





            }
        } catch (Throwable e) {

            Log.d("fusedLocationProvider", e.getMessage().toString());
        }

        new AsynUI(this).execute();

        // we build google api client



    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the LocationOnMap/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

          //  MenuItem searchViewItem = item.findItem(R.id.action_search);

            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    searchView.clearFocus();
             /*   if(list.contains(query)){
                    adapter.getFilter().filter(query);
                }else{
                    Toast.makeText(MainActivity.this, "No Match found",Toast.LENGTH_LONG).show();
                }*/
                    return false;

                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    //adapter.getFilter().filter(newText);
                    return false;
                }
            });

            return true;
        }

       if (id == R.id.action_share) {

           String text = "Hi install The Discout! save money and save Time! " +
                   "\n\n"+"https://play.google.com/store/apps/details?id=com.innoventia.thediscount";// R
          /* try {
               String text = "Hi install The Discout! save money and save Time! " +
                       "\n\n"+"https://play.google.com/store/apps/details?id=com.innoventia.thediscount";// Replace with your message.

              // String toNumber = phone; // Replace with mobile phone number without +Sign or leading zeros.

               Intent intent = new Intent(Intent.ACTION_VIEW);
               intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+toNumber +"&text="+text));
               startActivity(intent);
           }
           catch (Exception e){
               e.printStackTrace();
           }*/

           try {

               String whatsAppMessage = text;
               Intent sendIntent = new Intent();
               sendIntent.setAction(Intent.ACTION_SEND);
               sendIntent.putExtra(Intent.EXTRA_TEXT, whatsAppMessage);
               sendIntent.setType("text/plain");
               // Do not forget to add this to open whatsApp App specifically
               sendIntent.setPackage("com.whatsapp");
               startActivity(sendIntent);


           }

           catch (Exception e)
           {
               e.printStackTrace();
           }

       }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    getAddress(currentLocation);
                    Toast.makeText(MainActivity.this, currentLocation.getLatitude() + "  ********  " + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    Log.d("LATITUDE***", String.valueOf(currentLocation.getLatitude()));

                    Log.d("LONGITUDE***", String.valueOf(currentLocation.getLongitude()));

                } else {
                    Toast.makeText(MainActivity.this, "No Location recorded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        selectSideDrawer(item.getItemId());
        item.setChecked(true);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectSideDrawer(@IdRes int itemId)
    {

        Fragment changefragment=null;
        boolean isfragment=true;
        boolean isDuplicate=false;
        boolean isFirst=false;
        switch (itemId) {

            case R.id.nav_home:

                isDuplicate=changefragment instanceof Home;
                isFirst=true;
                changefragment=new Home();
                break;
            case R.id.nav_refer:
                isDuplicate=changefragment instanceof Refer;
                changefragment=new Refer();

                break;

            case R.id.nav_appoinment:
                isDuplicate=changefragment instanceof MyAppoinments;
                changefragment=new MyAppoinments();
                break;

            case R.id.nav_discount:
                isDuplicate=changefragment instanceof MyDiscount;
                changefragment=new MyDiscount();


            case R.id.nav_favourite:
                isDuplicate=changefragment instanceof MyFavourite;
                changefragment=new MyFavourite();
                break;

            case R.id.nav_rate:

                isfragment=false;

                  //show dialogue


                break;

            case R.id.nav_testimony:
                isDuplicate=changefragment instanceof Testimony;
                changefragment=new Testimony();

                break;
            case R.id.nav_about:
                isDuplicate=changefragment instanceof AboutUs;
                changefragment=new AboutUs();
                break;

            case R.id.nav_contact:
                isDuplicate=changefragment instanceof ContactUs;
                changefragment=new ContactUs();

                break;

            case R.id.nav_bhome:

                isDuplicate=changefragment instanceof Home;
                changefragment=new Home();

                break;

            case R.id.nav_sort:

                isfragment=false;

                  //show dialogue


                break;

            case R.id.nav_notf:
                isDuplicate=changefragment instanceof Testimony;
                changefragment=new Testimony();

                break;
            case R.id.nav_bfavourite:

                isDuplicate=changefragment instanceof MyFavourite;
                changefragment=new MyFavourite();

                break;

            case R.id.nav_chat:
                isDuplicate=changefragment instanceof ContactUs;
                changefragment=new ContactUs();

                break;



        }
        if(!isDuplicate&&isfragment)
        {
                changeFragment(changefragment,MainActivity.container,isFirst);
            Log.d("changed fragment",changefragment.getClass().getSimpleName());
         }

        isDuplicate=false;
        isfragment=true;

        //drawer.closeDrawer(GravityCompat.START);

    }


    @Override
    public void onDrawerSlide(@NonNull View view, float v) {

    }

    @Override
    public void onDrawerOpened(@NonNull View view) {

        changeFavouriteIconColor();

    }

    private void changeFavouriteIconColor() {

        PorterDuff.Mode mMode = PorterDuff.Mode.SRC_ATOP;

        navigationView.getMenu().getItem(4).getIcon().setColorFilter(Color.BLACK,mMode);
    }

    @Override
    public void onDrawerClosed(@NonNull View view) {

    }

    @Override
    public void onDrawerStateChanged(int i) {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

     /*   if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        // Permissions ok, we get last location
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location1!= null) {


        //   showToastAnywhere("Latitude_main: " + location1.getLatitude() + "\nLongitude : " + location1.getLongitude());
        }*/

        fetchLastLocation();
        startLocationUpdates();

    }

    private void startLocationUpdates() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           showToastAnywhere("You need to enable permissions to display location !");
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch(requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(MainActivity.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                } else {
                    if (googleApiClient != null) {
                        googleApiClient.connect();
                    }
                }

                break;

            case PERMISSION_LOCATION:

                if (grantResults.length == 2 && (grantResults[0] == PackageManager.PERMISSION_GRANTED || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    if (Utilities.hasLocationPermission(MainActivity.this, PERMISSION_LOCATION, true))
                        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            if (googleApiClient != null) {

                                googleApiClient.connect();


                            }
                            return;
                        }

                    //googleMap.setMyLocationEnabled(true);
                }


                break;
        }



    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location1) {

        if (location1 != null) {
          String loc= "Latitude_main_activity : " + location1.getLatitude() + "\nLongitude : " + location1.getLongitude();

          getAddress(location1);

        }

    }

    private void getAddress(Location location) {



        //GeoCoding

        


    }

    @Override
    public void OnLoadBackground() {


        // we add permissions we need to request location of the users
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(
                        new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
    }


        selectSideDrawer(R.id.nav_home);

}

    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {

        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;

    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;

    }


    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
           showToastAnywhere("You need to install Google Play Services to use the App properly");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (googleApiClient != null  &&  googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }


}
