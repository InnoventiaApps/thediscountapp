package com.innoventia.thediscount.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.TextView;

import com.alahammad.otp_view.OtpView;
import com.innoventia.thediscount.R;

public class UiUtils
{
    Context context;
    public static UiUtils getInstance(Context context)
    {

       return new UiUtils(context);

    }

    private UiUtils (Context context)
    {
        this.context=context;

    }
    public static int dpToPx(Context context,int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public void setTextViews(TextView[] textViews,String[] texts)
    {

        for (int i=0;i<textViews.length;i++)

        {  TextView textView =textViews[i];
            String text =texts[i];
           setTextViews(textView,text);
        }

    }

    public void setTextViews(TextView textView, String text)
    {

        textView.setText(text);

    }

    public String getStringResources(int res)
    {

        return context.getResources().getString(res) ;
    }


    public static boolean isEmpty(TextView[] textViews)
    {

        boolean isEmpty=false;

        for(int i=0;i<textViews.length;i++)
        {

            if(isEmpty=isEmpty(textViews[i]))
            {

                setError(textViews[i],R.string.msg_empty_error);

                return isEmpty;

            }


        }

       return isEmpty;
    }

    public static boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    /*


    public static TextView getEmptyText(TextView[] textViews)
    {

        TextView emptyText=null;

        for(int i=0;i<textViews.length;i++)
        {
            emptyText=textViews[i];

            if(isEmpty(emptyText))
            {

                return emptyText;

            }


        }

       return emptyText;
    }
     */


    public static String getText(TextView textView)
    {
        return textView.getText().toString().trim();
    }

    public static boolean isEmpty(TextView textView)
    {

        return TextUtils.isEmpty(getText(textView));

    }


    public static void setError(TextView textView,@StringRes int msg)
    {
        textView.setError(textView.getResources().getString(msg));
        textView.requestFocus();
        //textView.setError(getResources().getString(R.string.verify_captcha));


    }


}
