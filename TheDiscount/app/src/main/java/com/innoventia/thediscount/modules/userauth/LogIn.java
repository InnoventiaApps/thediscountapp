package com.innoventia.thediscount.modules.userauth;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.data.Preferences;
import com.innoventia.thediscount.main.activity.BaseActivity;
import com.innoventia.thediscount.main.activity.MainActivity;
import com.innoventia.thediscount.main.activity.UserAuthActivity;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.main.mulitithread.AsynUI;
import com.innoventia.thediscount.model.LoginModel;
import com.innoventia.thediscount.net.ApiUtils;
import com.innoventia.thediscount.testactvity;
import com.innoventia.thediscount.utils.UiUtils;
import com.innoventia.thediscount.utils.Utilities;
import com.innoventia.thediscount.utils.uihelper.AppTextView;
import com.innoventia.thediscount.utils.uihelper.UserAuthButton;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LogIn.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LogIn#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogIn extends BaseFragment implements OnBackgroundUIListner, OtpDialog.OnDiallogAcessListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    ImageView checkCaptcha;
    UserAuthButton userAuthButton;

    TextInputEditText txtEmail,txtPassword;
    GoogleApiClient googleApiClient;
    String TAG = LogIn.class.getSimpleName();
   /* String SITE_KEY = "6Ldq9VwUAAAAAMjMtK4ynpWNfXqGYUXU4j64U0_N";
    String SITE_SECRET_KEY = "6Ldq9VwUAAAAADS475M1FjgcbU2SP09Wh4WRHOEn";*/
    RequestQueue queue;
    Context context;
    AppTextView  txtForgot,txtVerification;
    boolean ifExists=false;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationStateChangedCallbacks;

    private DialogFragment dialogFragment;
    private boolean codeSent;
    private String verificationCode;
    private String phoneNumber;
    private static final int OTP_TIME_OUT_DURATION=60;
    public LogIn() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogIn.
     */
    // TODO: Rename and change types and number of parameters
    public static LogIn newInstance(String param1, String param2) {
        LogIn fragment = new LogIn();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        queue = Volley.newRequestQueue(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        baseView=inflater.inflate(R.layout.fragment_log_in, container, false);
        txtVerification=baseView.findViewById(R.id.txtVerification);
        txtEmail=baseView.findViewById(R.id.txtMail);
        txtPassword=baseView.findViewById(R.id.txtPsd);
        txtForgot=(AppTextView)baseView.findViewById(R.id.txtForgot);
        userAuthButton=baseView.findViewById(R.id.btnLogin);
        checkCaptcha=(ImageView) baseView.findViewById(R.id.checkImage);
        txtForgot.setVisibility(View.GONE);

         new AsynUI(context,LogIn.this);

        return baseView;


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
     /*   if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void OnLoadBackground() {

        setListeners();

    }

    private void setListeners() {

        userAuthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if( ((Animatable) checkCaptcha.getDrawable()).isRunning())
                {
                    ((Animatable) checkCaptcha.getDrawable()).stop();

                    if(!UiUtils.isEmpty(new TextInputEditText[]{txtEmail,txtPassword}))
                    {
                        if(UiUtils.isValidMail(UiUtils.getText(txtEmail)) && UiUtils.isValidMail(UiUtils.getText(txtPassword)))
                        {
                            getLoginDetails();//api login
                             ifExists = true;
                            txtForgot.setVisibility(View.GONE);
                            sendIntent(getActivity(), new Class[]{MainActivity.class, UserAuthActivity.class}, ifExists);
                        }

                        else
                        {
                            txtForgot.setVisibility(View.VISIBLE);

                        }

                    }

                }

                else
                {
                    txtVerification.setText("Failed to\n");
                    txtVerification.append(getString(R.string.verify_captcha));
                    txtVerification.setTextColor(Color.RED);
                  //  UiUtils.setError(checkCaptcha,R.string.verify_captcha);
                   // checkCaptcha.setError(getResources().getString(R.string.verify_captcha));

                }

            }
        });

     checkCaptcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               if( ((Animatable) checkCaptcha.getDrawable()).isRunning()) {

                   ((Animatable) checkCaptcha.getDrawable()).stop();// Code to display your message.
               }

                SafetyNet.getClient(getActivity()).verifyWithRecaptcha(getString(R.string.CAPTCHA_API_SITE_KEY)).addOnSuccessListener(getActivity(), new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse recaptchaTokenResponse) {

                        if (!recaptchaTokenResponse.getTokenResult().isEmpty()) {
                            handleCaptchaResult(recaptchaTokenResponse.getTokenResult());
                        }

                    }
                }).addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.d(TAG, "Error message: " +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d(TAG, "Unknown type of error: " + e.getMessage());
                        }

                    }
                });

            }
        });

       // checkCaptcha.setOnCheckedChangeListener(null);


        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAuth = FirebaseAuth.getInstance();

                phoneNumber=Preferences.getInstance(getActivity()).getLoginDetails().getMobile();

                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phoneNumber,                     // Phone number to verify
                        OTP_TIME_OUT_DURATION,                           // Timeout duration
                        TimeUnit.SECONDS,                // Unit of timeout
                        getActivity(),        // Activity (for callback binding)


                verificationStateChangedCallbacks= new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                        dismissDialogue(dialogFragment);
                        showToastAnywhere("Verification Completed");

                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {

                        dismissDialogue(dialogFragment);
                        showToastAnywhere("Verification Failed");

                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        super.onCodeSent(verificationId, forceResendingToken);

                        codeSent = true;
                        showToastAnywhere("Verification code has been send on your number");
                        verificationCode = verificationId;
                      //  mResendToken = token;
                        dialogFragment=((BaseActivity) getActivity()).openDialogueFragment(OtpDialog.newInstance(LogIn.this,verificationCode));

                    }



                    });



            }
        });

    }

    private void getLoginDetails() {
      //  Toast.makeText(LogIn.this, "11111"+UiUtils.getText(txtEmail)+txtPassword, Toast.LENGTH_SHORT).show();
        Call<LoginModel> call = ApiUtils.getLoginDetails(UiUtils.getText(txtEmail),UiUtils.getText(txtPassword));
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, retrofit2.Response<LoginModel> response) {
                if(response.code() == 404){
                    Toast.makeText(getActivity(), "Sorry for the delay.Please try after some time.", Toast.LENGTH_SHORT).show();
                }else {
                    LoginModel loginModel = (LoginModel) response.body();
                    Preferences.getInstance(getActivity()).setLoginDetails(loginModel);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {

             //   Log.d("isnetworkconnected", "" + Utilities.isNetworkConnected(testactvity.this));

                if (!Utilities.isNetworkConnected(getActivity())) {

                    Toast.makeText(getActivity(), "Please check network connectivity", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Sorry for the delay.Please try after some time!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void SigninWithPhone(PhoneAuthCredential credential) {

        codeSent = false;
        mAuth.signInWithCredential(credential).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                          changeFragment(new ForgotPassword(),UserAuthActivity.container,true);

                        }
                    },1000);
                    showToastAnywhere("Verification Complete");

                } else {
                    // Log.w(TAG, "signInWithCredential:failure", task.getException());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        showToastAnywhere("Invalid Verification");


                    }
                }


            }
        });

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onStart() {
        super.onStart();

       /* if (googleApiClient != null) {
            googleApiClient.connect();
        }*/
    }

    void handleCaptchaResult(final String responseToken) {
        String url = "https://www.google.com/recaptcha/api/siteverify";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getBoolean("success")){
                                ((Animatable) checkCaptcha.getDrawable()).start();
                               Toast.makeText(getActivity(),"You're not a Robot",Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception ex) {
                            Log.d(TAG, "Error message: " + ex.getMessage());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        ((Animatable) checkCaptcha.getDrawable()).stop();
                        Toast.makeText(getActivity(),"Error message: " + error.getMessage(),Toast.LENGTH_LONG).show();
                        Log.d(TAG, "Error message: " + error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", getString(R.string.CAPTCHA_API_SITE_SECRET_KEY));
                params.put("response", responseToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                80000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }
}




