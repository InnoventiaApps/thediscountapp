package com.innoventia.thediscount.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C9 on 9/26/2017.
 */

public class StatusModel {
    //  @SerializedName("status")
    //  int status;
    @SerializedName("errorCode")
    int errorCode;
    @SerializedName("errorMessage")
    String errorMessage;
    //  public boolean isSuccess() {
    //      return status==1;
    // }

    public StatusModel(int errorCode,String errorMessage){
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
