package com.innoventia.thediscount.modules.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.modules.view.adapter.Horizontal_AdListingAdapter;
import com.innoventia.thediscount.modules.view.adapter.Vertical_AdListingAdapter;
import com.innoventia.thediscount.utils.UiUtils;
import com.innoventia.thediscount.utils.uihelper.GridSpacingItemDecoration;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DiscountList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DiscountList extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String MerchantOffer = "MerchantOffer";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String getMerchantOffer;

RecyclerView recyclerView;
    public DiscountList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment DiscountList.
     */
    // TODO: Rename and change types and number of parameters
    public static DiscountList newInstance(String merchantOffer) {
        DiscountList fragment = new DiscountList();
        Bundle args = new Bundle();
        args.putString(MerchantOffer, merchantOffer);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            getMerchantOffer = getArguments().getString(MerchantOffer);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        baseView=inflater.inflate(R.layout.fragment_discount_list, container, false);
        recyclerView=baseView.findViewById(R.id.list);
        setRecyler();

        // Inflate the layout for this fragment
        return baseView;
    }
    private void setRecyler() {


        GridLayoutManager lLayout = new GridLayoutManager(getActivity(), 3);
        recyclerView.setHasFixedSize(true);
        int valueInPixels = (int) getResources().getDimension(R.dimen.padding_normal);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, UiUtils.dpToPx(context, valueInPixels), true));
        lLayout = new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setAdapter(new Vertical_AdListingAdapter(getActivity()));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }
}
