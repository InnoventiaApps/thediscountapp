package com.innoventia.thediscount.main.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.data.Preferences;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.main.mulitithread.AsynUI;

import java.net.URL;

import retrofit2.http.Url;

public class SplashActivity extends BaseActivity {


ProgressBar progressBar;
Handler handler=new Handler();
public static final int DURATION=5000;
    private int mProgressStatus = 0;
    int progressbarDuration=0;
    private ProgressBarAsync mProgressbarAsync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    //    setTheme(R.style.SplashTheme);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        progressBar=(ProgressBar)findViewById(R.id.ProgressBar);
        new ProgressBarAsync().execute();



    }

    @Override
    protected void onStart() {
        super.onStart();



    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    private class ProgressBarAsync extends AsyncTask<Void, Integer, Void> {

        private boolean mRunning = false;


        /** This callback method is invoked, before starting the background process */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mRunning = true;
        }

        /** This callback method is invoked on calling execute() method
         * on an instance of this class */
        @Override
        protected Void doInBackground(Void...params) {


            while(mProgressStatus<DURATION){
                try{
                    if(!mRunning)
                        break;
                    //500 is half a second... //1000 second
                    int sleepTime = mProgressStatus < 90 ? 10 : 500;
                    Thread.sleep(sleepTime);
                    mProgressStatus++;
                    /** Invokes the callback method onProgressUpdate */
                    publishProgress(mProgressStatus);


                }catch(Exception e){
                    Log.d("Exception", e.toString());
                }
            }
            return null;
        }

        /** This callback method is invoked when cancel() method is called
         * on an instance of this class */
        @Override
        protected void onCancelled() {
            super.onCancelled();
            mRunning = false;
        }

        /** This callback method is invoked when publishProgress()
         * method is called */
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(mProgressStatus);

        }

        /** This callback method is invoked when the background function
         * doInBackground() is executed completely */
        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);


            if(Preferences.getInstance(SplashActivity.this).isLoggedIn())
            {
                sendIntent(SplashActivity.this,MainActivity.class);

            }
            else {
                sendIntent(SplashActivity.this,UserAuthActivity.class);

            }

            finish();

        }
    }


}
