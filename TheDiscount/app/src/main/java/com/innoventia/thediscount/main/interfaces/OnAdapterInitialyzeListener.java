package com.innoventia.thediscount.main.interfaces;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.innoventia.thediscount.modules.view.adapter.GenericViewHolder;

import java.io.Serializable;
import java.util.List;


public interface OnAdapterInitialyzeListener<T,VH extends GenericViewHolder> extends Serializable {
 /*   void initialise(RecyclerView.Adapter adapter ,
                      OnAdapterResponseListener onAdapterResponseListener,
                      List<Object> list);*/

     int getFragmentLayout();
     void onBind();
     List<T> getNewList();
     VH getViewHolder();

}
