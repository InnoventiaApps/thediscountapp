package com.innoventia.thediscount.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by C9 on 9/28/2017.
 */

public class MerchantModel implements Serializable {

    @SerializedName("merchantName")
    String merchantName;
    @SerializedName("averageRating")
    String averageRating;
    @SerializedName("merchantId")
    String  merchantId;
    @SerializedName("address")
    String address;
    @SerializedName("latitude")
    String lattitude;
    @SerializedName("longitude")
    String longitude;


    public MerchantModel(String merchantName, String averageRating,String address,String merchantId) {
        this.merchantName = merchantName;
        this.averageRating = averageRating;
        this.address = address;
    }

    public MerchantModel(String address, String lattitude, String longitude) {
        this.address = address;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    public String getMerchantid(){
        return merchantId;
    }

    public long getMerchantId() {
        try {
            return Integer.parseInt(lattitude);
        } catch (Exception e) {
        }
        return -1;
    }

    public String getAddress() {
        return address;
    }

    public double getLat() {
        try {
            return Double.parseDouble(lattitude);
        } catch (Exception e) {
        }
        return 0;
    }

    public double getLng() {
        try {
            return Double.parseDouble(longitude);
        } catch (Exception e) {
        }
        return 0;
    }
    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }


    @Override
    public String toString() {
        return "MerchantModel{" +
                "merchantname='" + merchantName + '\'' +
                ", rating='" + averageRating + '\'' +
                ", address='" + address + '\'' +
                ", lattitude='" + lattitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
