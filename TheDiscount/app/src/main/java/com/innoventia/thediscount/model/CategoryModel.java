package com.innoventia.thediscount.model;

import com.google.gson.annotations.SerializedName;

public class CategoryModel
{
    @SerializedName("categoryName")
    String categoryName;
    @SerializedName("imageName")
    String categoryIcon;
    @SerializedName("userId")
    String userId;

    public CategoryModel(String categoryName, String categoryIcon, String userId) {
        this.categoryName = categoryName;
        this.categoryIcon = categoryIcon;
        this.userId = userId;
    }

    public CategoryModel(String categoryName, String categoryIcon) {
        this.categoryName = categoryName;
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String categoryId) {
        this.userId = categoryId;
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "categoryName='" + categoryName + '\'' +
                ", categoryIcon='" + categoryIcon + '\'' +
                ", userId=" + userId +
                '}';
    }
}
