package com.innoventia.thediscount.main.interfaces;

/**
 * Created by C9 on 9/25/2017.
 */

public interface OnAlertListener {
    void onAlertPositiveClicked(int requestId);
    void onAlertNegativeClicked(int requestId);
    void onAlertCancelled(int requestId);
}
