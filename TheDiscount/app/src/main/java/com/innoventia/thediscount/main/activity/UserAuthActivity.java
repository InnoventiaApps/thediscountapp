package com.innoventia.thediscount.main.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.innoventia.thediscount.R;
import com.innoventia.thediscount.data.Preferences;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.main.mulitithread.AsynUI;
import com.innoventia.thediscount.modules.userauth.UserOption;

public class UserAuthActivity extends BaseActivity{

   public static int container=R.id.actUserAuth;


    private String TAG=this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.UserAuthTheme);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_userauth);
        changeFragment(new UserOption(),container);


    }






}
