package com.innoventia.thediscount.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C9 on 9/26/2017.
 */

public class CommonModel<T> extends StatusModel {
    @SerializedName("data")
    T data;

    public CommonModel( T data) {
        super(0, "");
        this.data = data;
    }

    public CommonModel(int errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    public T getData() {
        return data;
    }
}
