package com.innoventia.thediscount.modules.userauth;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.utils.UiUtils;
import com.innoventia.thediscount.utils.uihelper.AppTextView;
import com.innoventia.thediscount.utils.uihelper.UserAuthButton;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPassword extends BaseFragment {


    private AppTextView txtPsd,txtRePsd;
    UserAuthButton resetButton;


    public ForgotPassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        baseView= inflater.inflate(R.layout.fragment_sign_up, container, false);

        txtPsd =(AppTextView) baseView.findViewById(R.id.txtPsd);
        txtRePsd =(AppTextView) baseView.findViewById(R.id.txtRePsd);
        resetButton =(UserAuthButton) baseView.findViewById(R.id.btnReset);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UiUtils.getText(txtPsd).equals(UiUtils.getText(txtRePsd)))
                {

                    //api for update user password

                }

                else {

                    UiUtils.setError(txtRePsd,R.string.psdMismatch);
                }

            }
        });
        return baseView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseView=null;
    }
}
