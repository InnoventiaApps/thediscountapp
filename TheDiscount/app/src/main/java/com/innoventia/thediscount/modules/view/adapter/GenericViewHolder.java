package com.innoventia.thediscount.modules.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

public abstract class GenericViewHolder<T> extends RecyclerView.ViewHolder
 {
       View itemView;

       public GenericViewHolder(@NonNull View itemView) {
           super(itemView);
           this.itemView = itemView;
            initialyseItem(itemView);

       }

       protected abstract void initialyseItem(View itemView);
     protected abstract void onBind(List<T> items,int adapterPosition);
   }
