package com.innoventia.thediscount.modules.view;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.MakeListFragment;
import com.innoventia.thediscount.model.CategoryModel;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.modules.view.adapter.GenericViewHolder;
import com.innoventia.thediscount.net.ApiUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeCategoryList extends MakeListFragment<CategoryModel,GenericViewHolder<CategoryModel>>
{
    List<CategoryModel> categoryModels=null;

   /* public static HomeCategoryList newInstance(Bundle args) {

        MakeListFragment fragment = new HomeCategoryList();

        return newInstance(fragment,args);
    }*/

    @Override
    public List<CategoryModel> loadList() {

        listCategory();
        return categoryModels;
    }

    @Override
    public int getLayout() {

        return R.layout.homecategory;
    }

    @Override
    public GenericViewHolder<CategoryModel> getViewHolder(View childView) {


       return new GenericViewHolder<CategoryModel>(childView) {

            TextView categoryName;
            ImageView categoryIcon;
            View itemView;


            @Override
            protected void initialyseItem(View itemView) {

                categoryName = itemView.findViewById(R.id.tvCategoryLabel);
                categoryIcon = itemView.findViewById(R.id.imagCategoryIcon);

            }

            @Override
            protected void onBind(List<CategoryModel> items, int adapterPosition) {

                try {
                    CategoryModel categoryModel = (CategoryModel) items.get(adapterPosition);
                    categoryName.setText(categoryModel.getCategoryName());
                    Log.d("CATEGORY_NAME = ", categoryModel.getCategoryName());
                    ApiUtils.loadImage(getActivity(),categoryModel.getCategoryIcon(), categoryIcon);

                }catch (Exception e){
                    e.printStackTrace();
                }



            }
        };

    }



    private void listCategory() {

        Call<CommonModelList<CategoryModel>> call= ApiUtils.getCategory();
        call.enqueue(new Callback<CommonModelList<CategoryModel>>() {
            @Override
            public void onResponse(Call<CommonModelList<CategoryModel>> call, Response<CommonModelList<CategoryModel>> response) {

                try {
                    Log.d("CATEGORY....", String.valueOf(response.code()));

                    categoryModels=response.body().getData();
                    //  swipeRefreshLayout.setRefreshing(false);

                    // Log.d("CATEGORY_RESPONSE", String.valueOf(response.body().getErrorCode()));
                    Log.d("CATEGORY_List", String.valueOf(categoryModels));


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CommonModelList<CategoryModel>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        listCategory();

                    }
                },3000);

            }
        });
    }


}



