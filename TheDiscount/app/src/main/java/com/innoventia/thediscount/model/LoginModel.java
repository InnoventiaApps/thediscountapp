package com.innoventia.thediscount.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginModel implements Serializable {

    @SerializedName("userId")
    String userId;
  /*  @SerializedName("userName")
    String userName;
    @SerializedName("firstName")
    String firstName;*/
    @SerializedName("email")
    String email;
    @SerializedName("phone")
    String phone;
    @SerializedName("token")
    String token;
    @SerializedName("referalCode")
    private String referalCode;



    public LoginModel(String userId,/*String userName,String firstName,*/String email, String phone, String token){
        this.userId = userId;
     //   this.userName = userName;
     //   this.firstName = firstName;
        this.email = email;
        this.phone = phone;
        this.token = token;
    }

   // first_name,last_name,phone,email,password,dob,zip_code,gender

    public String getuserId() {
        return userId;
    }

    public void setuserId(String userId) {
        this.userId = userId;
    }

   /* public String getuserName() {
        return userName;
    }

    public void setuserName(String userName) {
        this.userName = userName;
    }

    public String getfirstName() {
        return firstName;
    }

    public void setfirstName(String firstName) {
        this.firstName = firstName;
    }*/

    @Override
    public String toString(){
        return "LoginModel{"+
                "userId = "+ userId + '\'' +
              //  "userName = " + userName + '\'' +
              //  "firstName = " + firstName + '\'' +
                "email = " + email + '\'' +
                "phone = " + email + '\'' +
                "token = " + token + '\'';
    }
}
