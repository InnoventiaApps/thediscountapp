package com.innoventia.thediscount.modules.view;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.innoventia.thediscount.R;
import com.innoventia.thediscount.main.fragment.BaseFragment;
import com.innoventia.thediscount.main.fragment.MakeListFragment;
import com.innoventia.thediscount.main.interfaces.OnBackgroundUIListner;
import com.innoventia.thediscount.main.interfaces.OnBindLocationOnMapListListner;
import com.innoventia.thediscount.main.mulitithread.AsynUI;
import com.innoventia.thediscount.model.CategoryModel;
import com.innoventia.thediscount.model.CommonModelList;
import com.innoventia.thediscount.model.LocationModel;
import com.innoventia.thediscount.modules.view.adapter.GenericViewHolder;
import com.innoventia.thediscount.modules.view.adapter.HomeCategoryAdapter;
import com.innoventia.thediscount.net.ApiUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link Home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home extends BaseFragment implements  OnBindLocationOnMapListListner {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    static CommonModelList<LocationModel> locationModelsList;
    public static final String EXTRA_LOCATION="extra_location";

    RecyclerView recyclerView;
    private OnBackgroundUIListner mListener;
    GridLayoutManager lLayout;
    private TextView tvUserNameHome;
    List<CategoryModel> categoryModels;
    Fragment fragment;
    SwipeRefreshLayout swipeRefreshLayout;
    OnBackgroundUIListner mapListner,categoryListner;

    public Home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Home.
     */
    // TODO: Rename and change types and number of parameters
    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         try
         {
             baseView= inflater.inflate(R.layout.fragment_home, container, false);
           //  recyclerView=baseView.findViewById(R.id.list);
             tvUserNameHome=baseView.findViewById(R.id.tvUserNameHome);
          //   swipeRefreshLayout = baseView.findViewById(R.id.pullToRefresh);
             //swipeRefreshLayout.setOnRefreshListener(Home.this);
             //fragment=(Fragment) baseView.findViewById(R.id.fragmentHomeLocs);

            new AsynUI(new OnBackgroundUIListner() {
                @Override
                public void OnLoadBackground() {

                    try {
                        addCategory();

                    }

                    catch (Exception e) {
                        Log.d("addCategory",e.getMessage());
                        e.printStackTrace();
                    }

                }
            }).execute();

            new AsynUI(new OnBackgroundUIListner() {
                @Override
                public void OnLoadBackground() {
                    try {

                        getHomeLocation();

                    }

                    catch (Exception e) {
                        Log.d("addHomeLocations",e.getMessage());
                        e.printStackTrace();
                    }

                }
            }).execute();

           }
         catch (InflateException exception)
         {
                     if(mListener!=null) {
                         showToastAnywhere(R.string.msg_empty);
                     }
                   exception.printStackTrace();
                     Log.d("EXCEP//", exception.getMessage().toString());
         }


        return baseView;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context=context;

       if (context instanceof OnBackgroundUIListner) {
            mListener = (OnBackgroundUIListner) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnBackgroundUIListner");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void addCategory() {
        Bundle bundle=MakeListFragment.addBundle(getActivity(),7,3,false,true,true);
        addChildFragment(new Fragment[]{this, MakeListFragment.newInstance(new HomeCategoryList(),bundle)},R.id.fragmentCategory);
    }



    @Override
    public CommonModelList<LocationModel> getLocationList() {
        getHomeLocation();

        return locationModelsList;
    }


    private void getHomeLocation()
    {

        ApiUtils.getAllLocations(this,ApiUtils.getHomeLocation());
        if(LocationOnMap.commonlocationLists!=null&&LocationOnMap.commonlocationLists.getData().size()!=0)
         {
             addChildFragment(new Fragment[]{this,LocationOnMap.newInstance(LocationOnMap.commonlocationLists)},R.id.fragmentHomeLocs);
        }

    }

   /* private void getAllLocations() {

        Log.d("getlocation", "ss");

        Call<CommonModelList<LocationModel>> call= ApiUtils.getAllMerchants();

        call.enqueue(new Callback<CommonModelList<LocationModel>>() {
            @Override
            public void onResponse(Call<CommonModelList<LocationModel>> call, Response<CommonModelList<LocationModel>> response) {

                if(!isDetached()) {
                    try {
                        Log.d("getlocation",String.valueOf(response.code()));

                        locationModelsList= response.body();
                        Log.d("HomeOnResponse","LocationList="+locationModelsList.toString());
                        Log.d("locationModels", String.valueOf(response.body()));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(final Call<CommonModelList<LocationModel>> call, Throwable t) {

                Log.d("onFailure","Throwable="+t.getMessage());
                if(!isDetached()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(!isDetached()) {

                                locationModelsList=getLocationList();
                                //getAllLocations();
                            }
                        }
                    },3000);

                }
            }
        });

    }
*/
    private class CategoryViewHolder extends GenericViewHolder
    {
        TextView categoryName;
        ImageView categoryIcon;
        View itemView;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            initialyseItem(itemView);
        }

        @Override
        protected void initialyseItem(View itemView) {

            categoryName = itemView.findViewById(R.id.tvCategoryLabel);
            categoryIcon = itemView.findViewById(R.id.imagCategoryIcon);
        }

        @Override
        protected void onBind(List items, int adapterPosition) {

             try {
                CategoryModel categoryModel = (CategoryModel) items.get(adapterPosition);
                categoryName.setText(categoryModel.getCategoryName());
                Log.d("CATEGORY_NAME = ", categoryModel.getCategoryName());
                ApiUtils.loadImage(context,categoryModel.getCategoryIcon(), categoryIcon);

            }catch (Exception e){
                e.printStackTrace();
            }


        }


    }



  /*  @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        listCategory();
    }
*/
//    private void addHomeLocations() {
//
//        addChildFragment(new LocationOnMap(),R.id.fragmentHomeLocs);
//
//    }

  private void listCategory() {

        Call<CommonModelList<CategoryModel>> call= ApiUtils.getCategory();
        call.enqueue(new Callback<CommonModelList<CategoryModel>>() {
            @Override
            public void onResponse(Call<CommonModelList<CategoryModel>> call, Response<CommonModelList<CategoryModel>> response) {

                try {
                    Log.d("CATEGORY....", String.valueOf(response.code()));

                    categoryModels=response.body().getData();
                  //  swipeRefreshLayout.setRefreshing(false);

                   // Log.d("CATEGORY_RESPONSE", String.valueOf(response.body().getErrorCode()));
                    Log.d("CATEGORY_List", String.valueOf(categoryModels));
                    HomeCategoryAdapter rcAdapter = new HomeCategoryAdapter(getActivity(),categoryModels);
                    recyclerView.setAdapter(rcAdapter);
                    recyclerView.setNestedScrollingEnabled(true);
                    //  locationModels.toString()

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CommonModelList<CategoryModel>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                },3000);

            }
        });
    }





}








    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


